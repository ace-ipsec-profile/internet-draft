# IPsec profile of ACE

This is the working area for the Individual internet-draft, "STUN/TURN using PHP in Despair".

* [Editor's copy](https://santiag0aragon.gitlab.io/i_d_template/)
* [Individual Draft](https://tools.ietf.org/html/draft-0)
* [Compare Individual Draft and Editor's copy](https://tools.ietf.org/rfcdiff?url1=https://tools.ietf.org/id/draft-0.txt&url2=https://git@gitlab.com:santiag0aragon.github.io/i_d_template/draft-0.txt)


## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/git@gitlab.com:santiag0aragon/i_d_template/blob/master/CONTRIBUTING.md).

