#ifndef CBOR_WEB_TOKEN_H_
#define CBOR_WEB_TOKEN_H_


#include "cbor-encoder.h"



enum cbor_encoded_claim_key {
	ISS = 1,	/* Issuer -> string */
	SUB = 2,    /* Subject -> String */
	AUD = 3,    /* Audience -> string */
	EXP = 4,    /* Expiration time -> NumericDate */
	NBF = 5,    /* Not before -> NumericDate */
	IAT = 6,    /* Issued at -> NumericDate */
	CTI = 7,     /* CWT ID -> String */

	// ACE RELATED FIELDS https://tools.ietf.org/pdf/draft-ietf-ace-oauth-authz-06.pdf
	// section 5.5.5
	CLIENT_ID     = 8,  // String (Majot Type 3)
	// CLIENT_SECRET = 9,
	// RESPONSE_TYPE = 10,
	// REDIRECT_URL  = 11,
	SCOPE  		  = 12, // String (Majot Type 3)
	// STATE  		  = 13,
	// CODE  		  = 14,
	// ERROR  		  = 15,
	// ERROR_DES  	  = 16,
	// ERROR_URI  	  = 17,
	GRANT_TYPE    = 18, // uint  (Majot Type 0)
	ACCESS_TOKEN  = 19, // String (Majot Type 3)
	TOKEN_TYPE    = 20, // String (Majot Type 3)
	// EXPIRES_IN    = 21,
	USERNAME  	  = 22, // String (Majot Type 3)
	PASSWORD  	  = 23, // String (Majot Type 3)
	// REFRESH_TOKEN = 24,
	CNF  		  = 25,  // Map (Majot Type 5)
	PROFILE  	  = 26,  // String (Majot Type 3)
	// CNF_LEN	      = 27  // CNF len object
};

enum cnf_methods {
	COSE_KEY 	   = 1,
	COSE_ENCRYPTED = 2,
	CNF_KEY_ID	   = 3,
	IPSEC_STRUC		   = 4,
	KMP 		   = 5,
	// IPSEC_LEN	      = 6,  // IPsec len object
	// COSE_LEN	      = 7  // COSE len object
};

enum cose_key_methods {
	COSE_KEY_TYPE 	 = 1,
	COSE_KEY_ID 	 = 2,
	KEY		 = 3
};

enum ipsec{
	MODE		= 1,
	LIFE,
	IP_C,  //(IF MODE == TUNNEL)
	IP_RS,  //(IF MODE == TUNNEL)
	SPI_SA_C,
	SPI_SA_RS,
	IPSEC_ENC_ALG,
	IPSEC_PROT_TYPE,
	SEED
	// TODO Network Parameters
};


typedef struct cwt_struct {
	uint8_t *claim_key;
	uint8_t **claim;
	int8_t claim_count;
	int8_t max_count;
} cbor_web_token;

typedef struct ipsec_sa_struc {
	char mode[10];
	uint32_t life;
	char *ip_c;
	char *ip_rs;
	uint32_t spi_sa_c;
	uint32_t spi_sa_rs;
	uint32_t prot_type;
	uint32_t encr_alg;
	char *seed;
	uint8_t psk[32];
} ipsec_sa;

// int8_t add_issuer_claim(cbor_web_token *cbor_wtoken, char *iss);
// int8_t add_subject_claim(cbor_web_token *cbor_wtoken, char *sub);
// int8_t add_audiance_claim(cbor_web_token *cbor_wtoken, char *aud);
// int8_t add_expiration_time_claim(cbor_web_token *cbor_wtoken, uint32_t exp);
// int8_t add_not_before_claim(cbor_web_token *cbor_wtoken, uint32_t nbf);
// int8_t add_issued_at_claim(cbor_web_token *cbor_wtoken, uint32_t iat);
// int8_t add_cwt_id_claim(cbor_web_token *cbor_wtoken, char *cti);

// // TODO: add the corresponding add_* functions to all the claim_keys
// int8_t add_client_id(cbor_web_token *cbor_wtoken, char *client_id);
// int8_t add_scope(cbor_web_token *cbor_wtoken, char *scope);
// int8_t add_grant_type(cbor_web_token *cbor_wtoken, uint32_t grant_type);
// int8_t add_access_token(cbor_web_token *cbor_wtoken, char *access_token);
// int8_t add_token_type(cbor_web_token *cbor_wtoken, char *token_type);
// int8_t add_username(cbor_web_token *cbor_wtoken, char *username);
// int8_t add_password(cbor_web_token *cbor_wtoken, char *password);
// int8_t add_cnf(cbor_web_token *cbor_wtoken, cbor_data *cnf_data);
// int8_t add_profile(cbor_web_token *cbor_wtoken, char *profile);





void integer2bytestring(uint32_t data, uint8_t num_bytes, uint8_t *dstr);


void initiate_cbor_web_token(cbor_web_token *cbor_wtoken, int8_t num_claims);
void encode_cbor_web_token(cbor_data *data, cbor_web_token *cbor_wtoken);
void  decode_cbor_web_token(cbor_data *data,  ipsec_sa *new_ipsec_sa);//, cbor_web_token *cbor_wtoken);

void encode_cwt_directly(cbor_data *data, uint8_t claim_key, uint8_t *claim);
//void decode_cwtbor_web_token(cbor_data *data, cbor_web_token *cbor_wtoken);



void encode_cnf(cbor_data *data, uint8_t cnf_key, uint8_t *method);
void  decode_cnf(cbor_data *data,  ipsec_sa *new_ipsec_sa);//, cbor_web_token *cbor_wtoken)

void encode_cose_key(cbor_data *data, uint8_t ck_key, uint8_t *method);
void decode_cose_key(cbor_data *data, ipsec_sa *new_ipsec_sa);//, cbor_web_token *cbor_wtoken)

void encode_ipsec(cbor_data *data, uint8_t ipsec_key, uint8_t *method);
void decode_ipsec(cbor_data *data,  ipsec_sa *new_ipsec_sa);
#endif  /* CBOR_WEB_TOKEN_H_ */
