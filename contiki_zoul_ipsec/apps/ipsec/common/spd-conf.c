/**
 * \addtogroup ipsec
 * @{
 */

/**
 * \file
 *    SPD configuration
 *  \details
 *    This file contains functions for SPD configuration.
 *
 *    All values and definitions described herein pertains to RFC 4301
 *    (Security Architecture for IP) and
 *    RFC 5996 (Internet Key Exchange Protocol Version 2).
 *    Sections of special interests are:
 *      RFC 4301: 4.4.1 (Security Policy Database)
 *      RFC 5996: 3.3 (Security Association Payload)
 *
 *    Please see spd.h for a quick overview of the data format.
 * \author
 *      Vilhelm Jutvik <ville@imorgon.se>
 *    Runar Mar Magnusson <rmma@kth.se>
 */

/*
 * Copyright (c) 2012, Vilhelm Jutvik.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

#include "sa.h"
#include "spd.h"
#include "uip.h"
#include "spd-conf.h"

#if IKE_WITH_RPL
#include "rpl/rpl-ike-conf.h"
#include "rpl/rpl-spd-conf.h"
#endif

#if IKE_WITH_IEEE
#include "ieee-802-15-4/ieee-802-15-4-conf.h"
#include "ieee-802-15-4/ieee-802-15-4-spd-conf.h"
#endif

#define uip_ip6addr_set_val16(ip6addr, val) \
  ip6addr.u16[0] = val, \
  ip6addr.u16[1] = val, \
  ip6addr.u16[2] = val, \
  ip6addr.u16[3] = val, \
  ip6addr.u16[4] = val, \
  ip6addr.u16[5] = val, \
  ip6addr.u16[6] = val, \
  ip6addr.u16[7] = val

/**
 * IKEv2 proposals as described in RFC 5996 with the following exceptions:
 *
 * > Every proposal must offer integrity protection.
 *  This is provided through a combined mode transform _or_ via the integrity
 */
const spd_proposal_tuple_t spdconf_ike_proposal[6] =
{
  /* Either ENCR,INTEG,PRF,DH or ENCR+INTEG,PRF,DH CCM is a type of ENCR+INTEG*/
  { SA_CTRL_NEW_PROPOSAL, SA_PROTO_IKE },
  /* Encryption transform */
#ifdef IKE_ENCR
  { SA_CTRL_TRANSFORM_TYPE_ENCR, IKE_ENCR },
  { SA_CTRL_ATTRIBUTE_KEY_LEN, 16 },
#else
  { SA_CTRL_TRANSFORM_TYPE_ENCR, IKE_ENCR_DEFAULT },
  { SA_CTRL_ATTRIBUTE_KEY_LEN, 16 },
#endif

  /* Integrity transform */
#ifdef IKE_INTEG
  { SA_CTRL_TRANSFORM_TYPE_INTEG, IKE_INTEG },
#endif

  /* Psuedo-random function transform */
#ifdef IKE_PRF
  { SA_CTRL_TRANSFORM_TYPE_PRF, IKE_PRF },
#else
  { SA_CTRL_TRANSFORM_TYPE_PRF, IKE_PRF_DEFAULT },
#endif

  /* Diffie-Hellman group */
#ifdef IKE_DH
  { SA_CTRL_TRANSFORM_TYPE_DH, IKE_DH },
#else
  { SA_CTRL_TRANSFORM_TYPE_DH, IKE_DH_DEFAULT },
#endif

  /* Terminate the offer */
  { SA_CTRL_END_OF_OFFER, 0 }
};

const spd_proposal_tuple_t spdconf_ike_open_proposal[6] =
{
  /* IKE proposal */
  { SA_CTRL_NEW_PROPOSAL, SA_PROTO_IKE },
  { SA_CTRL_TRANSFORM_TYPE_ENCR, SA_ENCR_NULL },
  { SA_CTRL_TRANSFORM_TYPE_INTEG, SA_INTEG_AES_XCBC_MAC_96 },
  { SA_CTRL_TRANSFORM_TYPE_DH, SA_IKE_MODP_GROUP },
  { SA_CTRL_TRANSFORM_TYPE_PRF, SA_PRF_HMAC_SHA1 },
  /* Terminate the offer */
  { SA_CTRL_END_OF_OFFER, 0 }
};
/**
 * ESP proposal
 */
const spd_proposal_tuple_t my_ah_esp_proposal[5] =
{
  /* ESP proposal */
  { SA_CTRL_NEW_PROPOSAL, SA_PROTO_ESP },
#ifdef ESP_ENCR
  { SA_CTRL_TRANSFORM_TYPE_ENCR, ESP_ENCR },
  { SA_CTRL_ATTRIBUTE_KEY_LEN, 16 },
#else
  { SA_CTRL_TRANSFORM_TYPE_ENCR, EPS_ENCR_DEFAULT },
  { SA_CTRL_ATTRIBUTE_KEY_LEN, 16 },
#endif

#ifdef ESP_INTEG
  { SA_CTRL_TRANSFORM_TYPE_INTEG, ESP_INTEG },
#endif

  /* Terminate the offer */
  { SA_CTRL_END_OF_OFFER, 0 }
};

/**
 * Convenience preprocessor commands for creating the policy table
 */
#define set_ip6addr(direction, ip6addr) \
  .ip6addr_##direction##_from = ip6addr, \
  .ip6addr_##direction##_to = ip6addr

#define set_any_peer_ip6addr() \
  .peer_addr_from = &spd_conf_ip6addr_min, \
  .peer_addr_to = &spd_conf_ip6addr_max

#define set_any_peer_not_multi() \
  .peer_addr_from = &spd_conf_ip6addr_min, \
  .peer_addr_to = &spd_conf_ip6addr_multicast

#define set_any_peer_multi() \
  .peer_addr_from = &spd_conf_ip6addr_multicast, \
  .peer_addr_to = &spd_conf_ip6addr_max

#define set_my_port(port) \
  .my_port_from = port, \
  .my_port_to = port

#define set_any_my_port() \
  .my_port_from = 0, \
  .my_port_to = PORT_MAX

#define set_peer_port(port) \
  .peer_port_from = port, \
  .peer_port_to = port

#define set_any_peer_port() \
  .peer_port_from = 0, \
  .peer_port_to = PORT_MAX

/**
 * IP adresses that we use in policy rules.
 *
 * spd_conf_ip6addr_init() must be called prior to using the data structures in question
 */
uip_ip6addr_t spd_conf_ip6addr_min; /* Address :: */
uip_ip6addr_t spd_conf_ip6addr_max; /* Address ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff */
uip_ip6addr_t spd_conf_ip6addr_multicast; /* Address ff00:: */

uip_ip6addr_t authorization_server;
uip_ip6addr_t client_address;
uip_ip6addr_t resource_server;
uip_ip6addr_t authorization_server_minus_1;
uip_ip6addr_t authorization_server_plus_1;

/**
 * Setup of the SPD. This is where you as the user enters the security policy of your system.
 *
 * Adjust SPD_ENTRIES (in spd.h) according to need.
 */
spd_entry_t spd_table[SPD_ENTRIES] =
{
  /* BYPASS IKE traffic */
  {
    .selector =
    {
      set_any_peer_ip6addr(),               /* ...from any host... */
      .nextlayer_proto = UIP_PROTO_UDP,     /* ...using UDP... */
      set_my_port(500),                     /* ...to destination port 500. */
      set_any_peer_port()                   /* ...from any source port */
    },
    .proc_action = SPD_ACTION_BYPASS,       /* No protection necessary */
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
  },

  /* BYPASS mDNS traffic */
  {
    .selector =
    {
      set_any_peer_ip6addr(),               /* ...from any host... */
      .nextlayer_proto = UIP_PROTO_UDP,     /* ...using UDP... */
      set_my_port(5353),                     /* ...to destination port 500. */
      set_any_peer_port()                   /* ...from any source port */
    },
    .proc_action = SPD_ACTION_BYPASS,       /* No protection necessary */
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
  },
    {
    // Bypass ICMP traffic to be seen by DAG
    .selector =
    {
      set_any_peer_ip6addr(),
      .nextlayer_proto = UIP_PROTO_ICMP6,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_BYPASS,       /* No protection necessary */
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
  },
#if IKE_WITH_RPL
  {
    .selector =
    {
      set_any_peer_not_multi(),   /* Any source (incoming traffic), any destination (outgoing) */
      .nextlayer_proto = SPD_SELECTOR_NL_ANY_PROTOCOL,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_PROTECT,
    .offer = my_rpl_proposal,
    .security_protocol = SA_PROTO_RPL
  },
  {
    .selector =
    {
      set_any_peer_multi(),     /* Any multicast traffic */
      .nextlayer_proto = SPD_SELECTOR_NL_ANY_PROTOCOL,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_BYPASS,
    .offer = NULL,
    .security_protocol = 0
  },
#endif
#if IKE_WITH_IEEE
  {
    .selector =
    {
      set_any_peer_not_multi(),   /* Any source (incoming traffic), any destination (outgoing) */
      .nextlayer_proto = SPD_SELECTOR_NL_ANY_PROTOCOL,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_PROTECT,
    .offer = my_ieee_proposal,
    .security_protocol = SA_PROTO_IEEE_802_15_4
  },
  {
    .selector =
    {
      set_any_peer_multi(),     /* Any multicast traffic */
      .nextlayer_proto = SPD_SELECTOR_NL_ANY_PROTOCOL,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_BYPASS,
    .offer = NULL,
    .security_protocol = 0
  },
#endif

#if WITH_CONF_MANUAL_SA
  // Bypass UDP traffic to accept CoAP traffic
  {
    .selector =
    {
      set_any_peer_ip6addr(),
      .nextlayer_proto = UIP_PROTO_UDP,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_BYPASS,       /* No protection necessary */
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
  },
  #endif

  #if WITH_CONF_IPSEC_IKE

  {
    .selector =
    {
      .peer_addr_from = &client_address,
      .peer_addr_to = &client_address,
      .nextlayer_proto = UIP_PROTO_UDP,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_BYPASS,
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
    // .proc_action = SPD_ACTION_PROTECT,
    // .offer = my_ah_esp_proposal,
    // .security_protocol = SA_PROTO_ESP
  },
    {
    .selector =
    {
      .peer_addr_from = &resource_server,
      .peer_addr_to = &resource_server,
      .nextlayer_proto = UIP_PROTO_UDP,
      set_any_my_port(),
      set_any_peer_port()
    },
    // .proc_action = SPD_ACTION_PROTECT,
    // .offer = my_ah_esp_proposal,
    // .security_protocol = SA_PROTO_ESP

    .proc_action = SPD_ACTION_BYPASS,       /* No protection necessary */
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
  },
    {
    .selector =
    {
      .peer_addr_from = &authorization_server,
      .peer_addr_to = &authorization_server,
      .nextlayer_proto = UIP_PROTO_UDP,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_BYPASS,       /* No protection necessary */
    .offer = NULL,                           /* N/A */
    .security_protocol = 0
  },
   {
    .selector =
    {
      // .peer_addr_from = &authorization_server,
      // .peer_addr_to = &authorization_server,
      set_any_peer_ip6addr(),
      .nextlayer_proto = UIP_PROTO_UDP,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_PROTECT,       /* No protection necessary */
    .offer = my_ah_esp_proposal,
    .security_protocol = SA_PROTO_ESP
  },
// #endif
  {
    .selector =
    {
      set_any_peer_ip6addr(),   /* Any source (incoming traffic), any destination (outgoing) */
      .nextlayer_proto = SPD_SELECTOR_NL_ANY_PROTOCOL,
      set_any_my_port(),
      set_any_peer_port()
    },
    .proc_action = SPD_ACTION_DISCARD,
    .offer = NULL,
    .security_protocol = 0
  },
  #endif
};

void
spd_conf_init()
{
  uip_ip6addr_set_val16(spd_conf_ip6addr_min, 0x0);
  uip_ip6addr_set_val16(spd_conf_ip6addr_max, 0xffff);
  uip_ip6addr(&spd_conf_ip6addr_multicast,  0xff00, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0);
  uip_ip6addr(&authorization_server_minus_1, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x8eca);
  uip_ip6addr(&authorization_server,         0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x8ecb);
  uip_ip6addr(&authorization_server_plus_1,  0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x8ecc);
  uip_ip6addr(&client_address,               0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
  uip_ip6addr(&resource_server,              0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x4f16);
}
/** @} */
