/**
 * \addtogroup ipsec
 * @{
 */

/**
 * \file
 *    The SPD's interface
 * \author
 *		Vilhelm Jutvik <ville@imorgon.se>
 *
 */

/*
 * Copyright (c) 2012, Vilhelm Jutvik.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/** @} */

#include "lib/list.h"
#include "spd.h"
#include "ipsec.h"
#include "common-ipsec.h"
#include "spd-conf.h"
const spd_proposal_tuple_t my_ah_esp_proposal_1[5] =
{
  /* ESP proposal */
  { SA_CTRL_NEW_PROPOSAL, SA_PROTO_ESP },
#ifdef ESP_ENCR
  { SA_CTRL_TRANSFORM_TYPE_ENCR, ESP_ENCR },
  { SA_CTRL_ATTRIBUTE_KEY_LEN, 16 },
#else
  { SA_CTRL_TRANSFORM_TYPE_ENCR, EPS_ENCR_DEFAULT },
  { SA_CTRL_ATTRIBUTE_KEY_LEN, 16 },
#endif

#ifdef ESP_INTEG
  { SA_CTRL_TRANSFORM_TYPE_INTEG, ESP_INTEG },
#endif

  /* Terminate the offer */
  { SA_CTRL_END_OF_OFFER, 0 }
};


spd_entry_t *
spd_get_entry_by_addr(ipsec_addr_t *addr, uint8_t proto)
{
  uint8_t n;
  for(n = 0; n < SPD_ENTRIES; ++n) {
    if(ipsec_a_is_member_of_b(addr, (ipsec_addr_set_t *)&spd_table[n].selector)) {
      if(spd_table[n].security_protocol == proto || spd_table[n].security_protocol == 0) {
        // printf("\nSPD %d\n",n);
        return &spd_table[n];
      }
    }
  }
  IPSEC_PRINTF(IPSEC "Error: spd_get_entry_by_addr: Nothing found. You ought to have a final rule in the SPD table that catches all traffic. Please see the RFC.\n");
  return NULL;
}

void
spd_rs_entry_protect()
{
ipsec_addr_t to_search;
uip_ip6addr_t resource_server;
 uip_ip6addr(&resource_server, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x4f16);
 memcpy(&to_search.peer_addr, &resource_server, sizeof(uip_ip6addr_t));
 to_search.nextlayer_proto = 17;
 to_search.my_port = 0;
 to_search.peer_port = 0;
 // to_search->peer_addr = resource_server;
 // PRINTADDR(&to_search);
 spd_entry_t *spd_entry = spd_get_entry_by_addr(&to_search, 0);
 spd_entry->proc_action = SPD_ACTION_PROTECT;
 spd_entry->offer = my_ah_esp_proposal_1;
 spd_entry->security_protocol = SA_PROTO_ESP;

}
void
set_spd_rs_null()
{
ipsec_addr_t to_search;
uip_ip6addr_t resource_server;
 uip_ip6addr(&resource_server, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x4f16);
 memcpy(&to_search.peer_addr, &resource_server, sizeof(uip_ip6addr_t));
 to_search.nextlayer_proto = 17;
 to_search.my_port = 0;
 to_search.peer_port = 0;
 // to_search->peer_addr = resource_server;
 // PRINTADDR(&to_search);
 spd_entry_t *spd_entry = spd_get_entry_by_addr(&to_search, 0);
 IPSEC_PRINTF("\n\n============================================\n\n");
 PRINTFOUNDSPDENTRY(spd_entry);
 IPSEC_PRINTF("\n\n============================================\n\n");
 spd_entry->selector.peer_addr_from = NULL;
 spd_entry->selector.peer_addr_to = NULL;
}

void
set_spd_client_null()
{
printf("\n\n===== Setting Client SPD to null\n\n");
ipsec_addr_t to_search;
uip_ip6addr_t resource_server;
 uip_ip6addr(&resource_server, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
 memcpy(&to_search.peer_addr, &resource_server, sizeof(uip_ip6addr_t));
 to_search.nextlayer_proto = 17;
 to_search.my_port = 0;
 to_search.peer_port = 0;
 // to_search->peer_addr = resource_server;
 // PRINTADDR(&to_search);
 spd_entry_t *spd_entry = spd_get_entry_by_addr(&to_search, 0);
 IPSEC_PRINTF("\n\n============================================\n\n");
 PRINTFOUNDSPDENTRY(spd_entry);
 IPSEC_PRINTF("\n\n============================================\n\n");
 spd_entry->selector.peer_addr_from = NULL;
 spd_entry->selector.peer_addr_to = NULL;
}
void
spd_client_entry_bypass()
{
ipsec_addr_t to_search;
uip_ip6addr_t resource_server;
 uip_ip6addr(&resource_server, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
 memcpy(&to_search.peer_addr, &resource_server, sizeof(uip_ip6addr_t));
 to_search.nextlayer_proto = 17;
 to_search.my_port = 0;
 to_search.peer_port = 0;
 // to_search->peer_addr = resource_server;
 // PRINTADDR(&to_search);
 spd_entry_t *spd_entry = spd_get_entry_by_addr(&to_search, 0);
 spd_entry->proc_action = SPD_ACTION_BYPASS;
 spd_entry->offer = NULL;
 spd_entry->security_protocol = 0;

}

void
spd_client_entry_protect()
{
ipsec_addr_t to_search;
uip_ip6addr_t resource_server;
 uip_ip6addr(&resource_server, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
 memcpy(&to_search.peer_addr, &resource_server, sizeof(uip_ip6addr_t));
 to_search.nextlayer_proto = 17;
 to_search.my_port = 0;
 to_search.peer_port = 0;
 // to_search->peer_addr = resource_server;
 // PRINTADDR(&to_search);
 spd_entry_t *spd_entry = spd_get_entry_by_addr(&to_search, 0);
 spd_entry->proc_action = SPD_ACTION_PROTECT;
 spd_entry->offer = my_ah_esp_proposal_1;
 spd_entry->security_protocol = SA_PROTO_ESP;

}
/** @} */
