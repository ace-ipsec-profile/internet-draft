IPsec profile for ACE
===================================================

This is a proof-of-concept implementation of the IPsec profile of ACE described in the Internet Draft at https://tools.ietf.org/html/draft-aragon-ace-ipsec-profile-00

This implementation includes the Client in `ace-cliet.c`, the Resource Server in `ace-resource-server.c`and the Authorization Server in `ace-authorization-server.c`.
