/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Example resource
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdlib.h>
#include <string.h>
#include "er-coap-engine.h"
#include "rest-engine.h"




#include <stdio.h> /* For printf() */
#include "uip-udp-packet.h"
#include "uip.h"
#include "sad.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip.h"


#include "ace-token.h"

static void res_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(res_as_token,
         "title=\"Token Endpoint",
         res_get_handler,
         NULL,
         NULL,
         NULL);


// PROCESS ACCESS TOKEN GOT BY POST REQUEST
static void
res_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{


  printf("ACCESS TOKEN REQUEST RECEIVED\n");
  const uint8_t *payload = NULL;
  coap_get_payload(request, &payload);

  // coap_init_message(response, COAP_TYPE_CON, COAP_GET, 0);

  // const char *len = NULL;
  uint8_t payload_bytes[512];
  uint16_t payload_len;

  #if WITH_CONF_MANUAL_SA
  uint8_t mode = 1; // DP
  #endif
  #if WITH_CONF_IPSEC_IKE
  uint8_t mode = 2; // PSK
  #endif

  payload_len = generate_dummy_access_token(payload_bytes, mode);

    memcpy(buffer, payload_bytes, payload_len);
    coap_set_payload(response, buffer, payload_len);
    coap_set_header_content_format(response, APPLICATION_LINK_FORMAT);


  // char const *const payload_bytes = "ACCESS TOKEN RESPONSE\n";
  // int payload_len = strlen(payload_bytes);
  // coap_set_payload(response, &payload_bytes, payload_len);
  // if(REST.get_query_variable(request, "len", &len)) {
  //   payload_len = atoi(len);
  //   if(payload_len < 0) {
  //     payload_len = 0;
  //   }
  //   if(payload_len > REST_MAX_CHUNK_SIZE) {
  //     payload_len = REST_MAX_CHUNK_SIZE;
  //   }
  // memcpy(buffer, &payload_bytes, payload_len);
  // } else {
  //   memcpy(buffer, payload_bytes, payload_len);
  // }
  // REST.set_header_content_type(response, REST.type.TEXT_PLAIN);  text/plain is the default, hence this option could be omitted.
  // REST.set_header_etag(response, (uint8_t *)&payload_len, 1);
  // REST.set_response_payload(response, &payload_bytes, payload_len);
  printf("Sending ACCESS TOKEN RESPONSE %d\n",payload_len);
}

