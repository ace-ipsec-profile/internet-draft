/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Example resource
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdlib.h>
#include <string.h>
#include "er-coap-engine.h"
#include "rest-engine.h"




#include <stdio.h> /* For printf() */
#include "uip-udp-packet.h"
#include "uip.h"
// #include "ipsec.h"
// #include "sad-conf.c"
#include "sad.h"
// #include "ipsec.h"
// #include "ipsec-conf.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip.h"


#include "ace-token.h"
#include "ipsec_profile_utils.h"
static void res_post_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(res_rs_authz_info,
         "title=\"Authz-Info Endpoint",
         NULL,
         res_post_handler,
         NULL,
         NULL);


// PROCESS ACCESS TOKEN GOT BY POST REQUEST
static void
res_post_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{


  printf("POST TOKEN RECEIVED\n");
  const uint8_t *payload = NULL;
  coap_get_payload(request, &payload);

   cbor_data cb_data;
   cb_data.buf =  payload;
   cb_data.ptr = 0;

   ipsec_sa new_ipsec_sa;
   decode_cbor_web_token(&cb_data, &new_ipsec_sa);

  const char *len = NULL;
  char const *const message = "Token received\n";
  int length = strlen(message);
  if(REST.get_query_variable(request, "len", &len)) {
    length = atoi(len);
    if(length < 0) {
      length = 0;
    }
    if(length > REST_MAX_CHUNK_SIZE) {
      length = REST_MAX_CHUNK_SIZE;
    }
    memcpy(buffer, message, length);
  } else {
    memcpy(buffer, message, length);
  } REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_header_etag(response, (uint8_t *)&length, 1);
  REST.set_response_payload(response, buffer, length);

  #if WITH_CONF_MANUAL_SA
    process_direct_provisioning_token_rs(new_ipsec_sa);
  #endif
  #if WITH_CONF_IPSEC_IKE
    // process_psk_token_rs(new_ipsec_sa);
    set_psk(new_ipsec_sa.psk);
  #endif
  printf("\nRESPONDING...\n");
  printf("\n--------------------------------------------\n");

}

