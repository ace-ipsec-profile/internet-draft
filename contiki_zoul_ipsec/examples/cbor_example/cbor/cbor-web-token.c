#include "cbor-web-token.h"
#include <stdlib.h>



int8_t add_generic_claim(cbor_web_token *cbor_wtoken, uint8_t *claim,
						enum cbor_encoded_claim_key claim_key, uint8_t len) {

	if(cbor_wtoken->claim_count >= cbor_wtoken->max_count) {
		return 0;
	} else {
		cbor_wtoken->claim_key[cbor_wtoken->claim_count] = claim_key;
		cbor_wtoken->claim[cbor_wtoken->claim_count] = (uint8_t *)malloc(len*sizeof(uint8_t));
		memcpy(cbor_wtoken->claim[cbor_wtoken->claim_count], claim, len);
		//cbor_wtoken->claim[cbor_wtoken->claim_count][len] = '\0';
		printf("%d -> %s\n", cbor_wtoken->claim_count, cbor_wtoken->claim[cbor_wtoken->claim_count]);
		cbor_wtoken->claim_count++;
		return 1;
	}
}


void integer2bytestring(uint32_t data, uint8_t num_bytes, uint8_t *dstr) {

	uint8_t v, i;
	for(i=num_bytes; i>=1; i--) {
		v = (uint8_t) (data%256);
		data = (uint32_t) (data - v) / 256;
		dstr[i-1] = v;
	}
	//data = (uint32_t)dstr[0]*16777216 + (uint32_t)dstr[1]*65536 + (uint32_t)dstr[2]*256 + (uint32_t)dstr[3];
	//printf("%lu -> %u, %u, %u, %u\n", data, dstr[0], dstr[1], dstr[2], dstr[3]);
}


uint32_t bytestring2integer(uint8_t num_bytes, uint8_t *dstr) {

	uint32_t data;
	uint8_t  i;

	data  = (uint32_t) dstr[0];
	for(i=1; i<num_bytes; i++) {
		data = (data << 8);
		data = data + (uint32_t) dstr[i];
	}

	return data;
}


int8_t add_integer_claim(cbor_web_token *cbor_wtoken, uint32_t claim,
						enum cbor_encoded_claim_key claim_key, uint8_t len) {

	uint8_t dstr[len];
	integer2bytestring(claim, len, dstr);

	return add_generic_claim(cbor_wtoken, dstr, EXP, len);
}

int8_t add_issuer_claim(cbor_web_token *cbor_wtoken, char *iss) {

	return add_generic_claim(cbor_wtoken, (uint8_t *)iss, ISS, strlen(iss));
}

int8_t add_subject_claim(cbor_web_token *cbor_wtoken, char *sub) {

	return add_generic_claim(cbor_wtoken, (uint8_t *)sub, SUB, strlen(sub));
}

int8_t add_audiance_claim(cbor_web_token *cbor_wtoken, char *aud) {

	return add_generic_claim(cbor_wtoken, (uint8_t *)aud, AUD, strlen(aud));
}

int8_t add_expiration_time_claim(cbor_web_token *cbor_wtoken, uint32_t exp) {

	return add_integer_claim(cbor_wtoken, exp, EXP, 4);
}

int8_t add_not_before_claim(cbor_web_token *cbor_wtoken, uint32_t nbf) {

	return add_integer_claim(cbor_wtoken, nbf, NBF, 4);
}

int8_t add_issued_at_claim(cbor_web_token *cbor_wtoken, uint32_t iat) {

	return add_integer_claim(cbor_wtoken, iat, IAT, 4);
}

int8_t add_cwt_id_claim(cbor_web_token *cbor_wtoken, char *cti) {

	return add_generic_claim(cbor_wtoken, (uint8_t *)cti, CTI, strlen(cti));
}
// TODO: add the corresponding add_* functions to all the claim_keys

int8_t add_client_id(cbor_web_token *cbor_wtoken, char *client_id){
	return add_generic_claim(cbor_wtoken, (uint8_t *)client_id, CLIENT_ID, strlen(client_id));
}
int8_t add_scope(cbor_web_token *cbor_wtoken, char *scope){
	return add_generic_claim(cbor_wtoken, (uint8_t *)scope, SCOPE, strlen(scope));
}
int8_t add_grant_type(cbor_web_token *cbor_wtoken, uint32_t grant_type){
	return add_integer_claim(cbor_wtoken, grant_type, GRANT_TYPE, 4);
}
int8_t add_access_token(cbor_web_token *cbor_wtoken, char *access_token){
	return add_generic_claim(cbor_wtoken, access_token, ACCESS_TOKEN, strlen(access_token));
}
int8_t add_token_type(cbor_web_token *cbor_wtoken, char *token_type){
	return add_generic_claim(cbor_wtoken, token_type, TOKEN_TYPE, strlen(token_type));
}
int8_t add_username(cbor_web_token *cbor_wtoken, char *username){
	return add_generic_claim(cbor_wtoken, (uint8_t *)username, USERNAME, strlen(username));
}
int8_t add_password(cbor_web_token *cbor_wtoken, char *password){
	return add_generic_claim(cbor_wtoken, (uint8_t *)password, PASSWORD, strlen(password));
}
int8_t add_cnf(cbor_web_token *cbor_wtoken, cbor_data *cnf_data){
// TODO define function
	printf("%s\n","TODO: CNF\n" );
	return add_generic_claim(cbor_wtoken, (uint8_t *)cnf_data, CNF, strlen(cnf_data));
}
int8_t add_profile(cbor_web_token *cbor_wtoken, char *profile){
	return add_generic_claim(cbor_wtoken, (uint8_t *)profile, PROFILE, strlen(profile));

}


/***********************************************************************/

void initiate_cbor_web_token(cbor_web_token *cbor_wtoken, int8_t num_claims) {

	cbor_wtoken->claim_key = (uint8_t *)malloc(num_claims*sizeof(uint8_t));
	cbor_wtoken->claim = (uint8_t **)malloc(num_claims*sizeof(uint8_t));

	cbor_wtoken->claim_count = 0;
	cbor_wtoken->max_count = num_claims;
}



void encode_cwt_directly(cbor_data *data, uint8_t claim_key, uint8_t *claim) {
	encode_unsigned_int(data, claim_key);
	switch(claim_key) {
		case ISS:
		  	encode_text_string(data, (char *)claim);
         	break;
		case SUB:
		  	encode_text_string(data, (char *)claim);
         	break;
		case AUD:
		  	encode_text_string(data, (char *)claim);
         	break;
		case CTI:
		 	encode_text_string(data, (char *)claim);
         	break;
		case EXP:
			encode_major_type_six(data, 1, claim);
			break;
		case NBF:
			encode_major_type_six(data, 1, claim);
			break;
		case IAT:
			encode_major_type_six(data, 1, claim);
			break;
		case CLIENT_ID:
		 	encode_text_string(data, (char *)claim);
         	break;
		case SCOPE:
		 	encode_text_string(data, (char *)claim);
         	break;
		case GRANT_TYPE:
			encode_major_type_six(data, 1, claim);
			break;
		case TOKEN_TYPE:
		 	encode_text_string(data, (char *)claim);
         	break;
		case ACCESS_TOKEN:
		 	encode_text_string(data, (char *)claim);
         	break;
		case USERNAME:
		 	encode_text_string(data, (char *)claim);
         	break;
		case PASSWORD:
		 	encode_text_string(data, (char *)claim);
         	break;
		case CNF:
			// TODO pass len of cnf
			encode_byte_string(data, (uint8_t *)claim, 206);
			break;
		case PROFILE:
		 	encode_text_string(data, (char *)claim);
			break; // TODO find a better way to see if the key is in enum
		default:
			printf("ENCODING ERROR:  unknown claim key: %d\n", claim_key);
	}
}

void encode_cnf(cbor_data *data, uint8_t cnf_key, uint8_t *method) {
	encode_unsigned_int(data, cnf_key);
	switch(cnf_key) {
		case COSE_KEY:
			encode_byte_string(data, (uint8_t *)method, 34);
         	break;
		case COSE_ENCRYPTED:
		  	encode_text_string(data, (char *)method);
         	break;
		case CNF_KEY_ID:
		  	encode_text_string(data, (char *)method);
         	break;
		case IPSEC:
			encode_byte_string(data, (uint8_t *)method, 86);
         	break;
		case KMP:
			encode_text_string(data, (char *)method);
         	break;
		default:
			printf("ENCODING ERROR:  unknown cnf key: %d\n", cnf_key);
	}
}

void decode_cnf(cbor_data *data) {

	uint16_t len;
	uint8_t  key;
	uint8_t  i;

	uint8_t claim[40];

	uint8_t ipsec_bytes[256];
	uint16_t ipsec_len;
	cbor_data ipsec_payload;

	uint8_t cose_key_bytes[256];
	uint16_t cose_key_len;
	cbor_data cose_key_payload;

	len = (uint16_t)decode_map_array(data);

	// printf("\n\tcose_key{ \n");

	for(i=0; i<len; i++) {
		key = (uint8_t) decode_unsigned_int(data);

		switch(key) {
		case COSE_KEY:
		  	printf("\t\tCOSE_Key:{\n");
			cose_key_len = decode_byte_string(data, cose_key_bytes);
 			cose_key_payload.buf = cose_key_bytes;
 			cose_key_payload.ptr = 0;
			decode_cose_key(&cose_key_payload);
			printf("\t\t}\n");
			break;

		case COSE_ENCRYPTED:
		  	decode_text_string(data, (char *)claim);
			printf("\t\tCOSE_Encrypted: %s\n", claim);
         	break;
		case CNF_KEY_ID:
		  	decode_text_string(data, (char *)claim);
			printf("\t\tkid: %s\n", claim);
         	break;
		case IPSEC:
				printf("\t\tipsec:{\n");
				ipsec_len = decode_byte_string(data, ipsec_bytes);
	 			ipsec_payload.buf = ipsec_bytes;
	 			ipsec_payload.ptr = 0;
				decode_ipsec(&ipsec_payload);
				printf("\t\t}\n");
				break;
			printf("\t\tTODO ipsec\n");
         	break;
		case KMP:
			decode_text_string(data, (char *)claim);
			printf("\t\tkmp: %s\n", claim);
         	break;

		default:
			printf("DECODING ERROR: unknown cnf value\n");

		}
	}
	// printf("\n\t} //cose_key \n");
}
void encode_cose_key(cbor_data *data, uint8_t ck_key, uint8_t *method) {
	encode_unsigned_int(data, ck_key);
	switch(ck_key) {
		case COSE_KEY_TYPE:
		  	encode_text_string(data, (char *)method);
         	break;
		case COSE_KEY_ID:
		  	encode_text_string(data, (char *)method);
         	break;
		case KEY:
		  	encode_text_string(data, (char *)method);
         	break;
		default:
			printf("ENCODING ERROR: unknown COSE_Key key: %d\n", ck_key);
	}
}
void decode_cose_key(cbor_data *data) {

	uint16_t len;
	uint8_t  key;
	uint8_t  i;

	uint8_t claim[30];

	len = (uint16_t)decode_map_array(data);

	// printf("\n\tcose_key{ \n");

	for(i=0; i<len; i++) {
		key = (uint8_t) decode_unsigned_int(data);

		switch(key) {
		case COSE_KEY_TYPE:
		  	decode_text_string(data, (char *)claim);
			printf("\t\t\tkty: %s\n", claim);
         	break;
		case COSE_KEY_ID:
		  	decode_text_string(data, (char *)claim);
			printf("\t\t\tkid: %s\n", claim);
         	break;
		case KEY:
		  	decode_text_string(data, (char *)claim);
			printf("\t\t\tkey: %s\n", claim);
         	break;
		default:
			printf("DECODING ERROR: unknown COSE_Key value\n");

		}
	}
	// printf("\n\t} //cose_key \n");
}
void encode_ipsec(cbor_data *data, uint8_t ipsec_key, uint8_t *method) {
	encode_unsigned_int(data, ipsec_key);
	switch(ipsec_key) {
		case MODE:
		  	encode_text_string(data, (char *)method);
         	break;
		case PROTOCOL:
		  	encode_text_string(data, (char *)method);
         	break;
		case LIFE:
			encode_major_type_six(data, 1, method);
         	break;
		case IP_C:
		  	encode_text_string(data, (char *)method);
         	break;
		case IP_RS:
		  	encode_text_string(data, (char *)method);
         	break;
		case SPI_SA_C:
			encode_major_type_six(data, 1, method);
         	break;
		case SPI_SA_RS:
			encode_major_type_six(data, 1, method);
         	break;
		case IPSEC_ALG:
		  	encode_text_string(data, (char *)method);
         	break;
		case SEED:
		  	encode_text_string(data, (char *)method);
         	break;
		default:
			printf("DECODING ERROR: unknown ipsec key: %d\n", ipsec_key);
	}
}

void decode_ipsec(cbor_data *data) {

	uint16_t len;
	uint8_t  key;
	uint32_t time;
	uint8_t  i;

	uint8_t claim[30];

	len = (uint16_t)decode_map_array(data);

	// printf("\n\tipsec{ \n");

	for(i=0; i<len; i++) {
		key = (uint8_t) decode_unsigned_int(data);

		switch(key) {
		case MODE:
			decode_text_string(data, (char *)claim);
			printf("\t\t\tmode: %s\n", claim);
         	break;
		case PROTOCOL:
			decode_text_string(data, (char *)claim);
			printf("\t\t\tprotocol: %s\n", claim);
         	break;
		case LIFE:
			decode_major_type_six(data, 1, claim);
			time = bytestring2integer(4, claim);
			printf("\t\t\tlife: %lu\n", time);
         	break;
		case IP_C:
			decode_text_string(data, (char *)claim);
			printf("\t\t\tip_c: %s\n", claim);
         	break;
		case IP_RS:
			decode_text_string(data, (char *)claim);
			printf("\t\t\tip_rs: %s\n", claim);
         	break;
		case SPI_SA_C:
			decode_major_type_six(data, 1, claim);
			time = bytestring2integer(4, claim);
			printf("\t\t\tspi_sa_c: %lu\n", time);
         	break;
		case SPI_SA_RS:
			decode_major_type_six(data, 1, claim);
			time = bytestring2integer(4, claim);
			printf("\t\t\tspi_sa_c: %lu\n", time);
         	break;
		case IPSEC_ALG:
			decode_text_string(data, (char *)claim);
			printf("\t\t\talg: %s\n", claim);
         	break;
		case SEED:
			decode_text_string(data, (char *)claim);
			printf("\t\t\tseed: %s\n", claim);
         	break;


			default:
				printf("DECODING ERROR: unknown ipsec value\n");
		}
	}
	// printf("\n\t} //ipsec \n");
}
void encode_cbor_web_token(cbor_data *data, cbor_web_token *cbor_wtoken) {

	uint8_t i;

	encode_map_array(data, cbor_wtoken->claim_count);

	for(i=0; i<cbor_wtoken->claim_count; i++) {
		encode_cwt_directly(data, cbor_wtoken->claim_key[i], cbor_wtoken->claim[i]);
	}
}



void decode_cbor_web_token(cbor_data *data) {

	uint16_t len;
	uint8_t  key;
	uint32_t time;
	uint8_t  i;

	uint8_t claim[40]; // old value 30

	uint8_t cnf_bytes[256];
	uint16_t cnf_len;
	cbor_data cnf_payload;

	len = (uint16_t)decode_map_array(data);

	printf("\nCWT: %u claims\n", len);

	for(i=0; i<len; i++) {
		key = (uint8_t) decode_unsigned_int(data);
		switch(key) {
			case ISS:
				decode_text_string(data, (char *)claim);
				printf("\tiss: %s\n", claim);
				break;
			case SUB:
				decode_text_string(data, (char *)claim);
				printf("\tsub: %s\n", claim);
				break;
			case AUD:
				decode_text_string(data, (char *)claim);
				printf("\taud: %s\n", claim);
				break;
			case EXP:
				decode_major_type_six(data, 1, claim);
				time = bytestring2integer(4, claim);
				printf("\texp: %lu\n", time);
				break;
			case NBF:
				decode_major_type_six(data, 1, claim);
				time = bytestring2integer(4, claim);
				printf("\tnbf: %lu\n", time);
				break;
			case IAT:
				decode_major_type_six(data, 1, claim);
				time = bytestring2integer(4, claim);
				printf("\tiat: %lu\n", time);
				break;
			case CTI:
				decode_text_string(data, (char *)claim);
				printf("\tcti: %s\n", claim);
				break;
			case CLIENT_ID:
				decode_text_string(data, (char *)claim);
				printf("\tclient_id: %s\n", claim);
				break;
			case SCOPE:
				decode_text_string(data, (char *)claim);
				printf("\tscope: %s\n", claim);
				break;
			case GRANT_TYPE:
				decode_major_type_six(data, 1, claim);
				time = bytestring2integer(4, claim);
				printf("\tgrant_type: %d\n", time);
				break;
			case ACCESS_TOKEN:
				decode_text_string(data, (char *)claim);
				printf("\taccess_token: %s\n", claim);
				break;
			case TOKEN_TYPE:
				decode_text_string(data, (char *)claim);
				printf("\ttoken_type: %s\n", claim);
				break;
			case USERNAME:
				decode_text_string(data, (char *)claim);
				printf("\tusername: %s\n", claim);
				break;
			case PASSWORD:
				decode_text_string(data, (char *)claim);
				printf("\tpassword: %s\n", claim);
				break;
			case CNF:
				printf("\tcnf:{\n");
				cnf_len = decode_byte_string(data, cnf_bytes);
	 			cnf_payload.buf = cnf_bytes;
	 			cnf_payload.ptr = 0;
				decode_cnf(&cnf_payload);
				printf("\t}\n");
				break;
			case PROFILE:
				decode_text_string(data, (char *)claim);
				printf("\tprofile: %s\n", claim);
				break;
			default:
				printf("DECODING ERROR: unknown claim\n");
		}
	}
}
