#include "cose-encoder.h"

#include "lib/ccm-star.h"

#include "stdlib.h"

#include "ecc.h"


#define WORDS_32_BITS 1

static ecc_point_a public_alice,public_bob; 
static u_word private_alice[NUMWORDS],private_bob[NUMWORDS];
//static uint32_t *address = 0x20000240; // from .map file
//static uint32_t *address_end = 0x20002a40; // from .map file




const unsigned char COSE_SECRET[] = "CHAYAN@SICS#KEY2";	

const unsigned char nonce[13] = {0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0x20, 0x21, 0x22, 0x23};

const unsigned char a[2] = {0x10, 0x20};
unsigned char a_len = 2;
unsigned char result[64];
unsigned char mic_len = 16;
int forward = 1;
	
const struct ccm_star_driver CCM_STAR;





/********************** private functions ****************************/

void encode_cose_header_map_array(cbor_data *data, cose_header *cose_hdr) {
	
	unsigned char i;
	
	encode_map_array(data, cose_hdr->map_len);

	for(i=0; i<cose_hdr->map_len; i++) {
		/* enter the label */
		encode_unsigned_int(data, cose_hdr->label[i]);
		
		/* enter the value */
		encode_text_string(data, (char *)cose_hdr->value[i]);
	}
}


void encode_cose_header(cbor_data *data, cose_header *cose_hdr) {
	
	cbor_data cbd;
	cbd.buf = &data->buf[data->ptr+8];
	cbd.ptr = 0;
	encode_cose_header_map_array(&cbd, cose_hdr);
	
	if(cose_hdr->protected == 1) {
		/* encrypt the header */    
		CCM_STAR.set_key(COSE_SECRET);
		CCM_STAR.aead(nonce, cbd.buf, cbd.ptr, a, a_len, result, mic_len, forward);
	
		/* protected header: add the encrypted header as byte string */
		encode_byte_string(data, cbd.buf, cbd.ptr);
		
		/* unprotected header: add a map of length 0 */
		encode_map_array(data, 0);
	}
	else {
		/* protected header: add a byte_string of length 0 */
		encode_byte_string(data, cbd.buf, 0);
	
		/* unprotected header: just add already encoded cose header */
		memcpy(&data->buf[data->ptr], cbd.buf, cbd.ptr);
		data->ptr = data->ptr + cbd.ptr;
	}
}


/**
 * 
 */
uint8_t decode_cose_header_map_array(cbor_data *data, cose_header *cose_hdr) {
	
	uint8_t i;
	uint8_t label;
	
	cose_hdr->map_len = (uint8_t) decode_map_array(data);
	
	for(i=0; i<cose_hdr->map_len; i++) {
		label = (uint8_t) decode_unsigned_int(data);
		cose_hdr->label[i] = label;
		decode_text_string(data, (char *)cose_hdr->value[i]);
		
		switch(label) {
			case ALG:
				printf("ALG: %s\n", (char *)cose_hdr->value[i]);
				break;
			case CRIT:
				printf("CRIT: %s\n", (char *)cose_hdr->value[i]);
				break;
			case CON_TYPE:
				printf("CON_TYPE: %s\n", (char *)cose_hdr->value[i]);
				break;
			case KEY_ID:
				printf("KEY_ID: %s\n", (char *)cose_hdr->value[i]);
				break;
			case IV:
				printf("IV: %s\n", (char *)cose_hdr->value[i]);
				break;
			case PAR_IV:
				printf("PAR_IV: %s\n", (char *)cose_hdr->value[i]);
				break;
			case CNT_SIGN:
				printf("CNT_SIGN: %s\n", (char *)cose_hdr->value[i]);
				break;
			default:
				printf("unknown cose header label\n");
				return 0;
		}
	}
	
	return 1;
}

/**
 * 
 */
uint8_t decode_cose_header(cbor_data *data, cose_header *cose_hdr) {

	/* protected header: decode the header */
	uint8_t len = (uint8_t) decode_byte_string(data);
	
	if(len > 0)  {
		printf("Protected header \n");
		
		/* decrypt the header */    
		CCM_STAR.set_key(COSE_SECRET);
		CCM_STAR.aead(nonce, &data->buf[data->ptr], len, a, a_len, result, mic_len, forward);
		
		/* decode the cose header, which is a map array */
		decode_cose_header_map_array(data, cose_hdr);
		cose_hdr->protected = 1;
		
		/* unprotected header: decode the header, which should have a length 0 */
		len = decode_map_array(data);
	}
	else {
		printf("Unprotected header \n");
		
		decode_cose_header_map_array(data, cose_hdr);
		cose_hdr->protected = 0;
	}
	
	return 1;
}

/**
 * 
 */
void calculate_signature(uint8_t *payload, uint8_t *payload_len, uint8_t *signature) {
	
	static u_word shared[NUMWORDS];
	static u_word r_x[NUMWORDS];
	
	new_ecc_init();

	//printf("Alice Private Key Generation\n");
	ecc_generate_private_key(private_alice);
	//bigint_print(private_alice, NUMWORDS);

	//printf("Bob Private Key Generation\n");
	ecc_generate_private_key(private_bob);
	//bigint_print(private_bob, NUMWORDS);

	//printf("Alice Public Key Generation\n");
	ecc_generate_public_key(private_alice, &public_alice);
	//bigint_print(&public_alice, NUMWORDS);
	
	//printf("Bob Public Key Generation\n");
	ecc_generate_public_key(private_bob, &public_bob);
	//bigint_print(&public_bob, NUMWORDS);
	
	//printf("Alice Shared Key Generation (Alice private key + Bob public key)\n");
	ecc_generate_shared_key(shared, private_alice, &public_bob);
	
	//printf("Shared secret generated by Alice:\n");
	//bigint_print(shared, NUMWORDS);
	
	
	//printf("Alice signs message \"Hello Bob\"\n");
	ecc_generate_signature(&private_alice, (const uint8_t *)"Hello Bob", 9,
					   signature, r_x);
}

void varify_signature(uint8_t *payload, uint8_t *payload_len, uint8_t *signature) {
	
	static u_word shared[NUMWORDS];
	static u_word r_x[NUMWORDS];
	
	//printf("Bob Shared Key Generation (Bob private key + Alice public key)\n");
	ecc_generate_shared_key(shared, private_bob, &public_alice);
	
	
	if(ecc_check_signature(&public_alice, (const uint8_t *) "Hello Bob", 9,
					signature, r_x)) {
		printf("The signature is valid for the provided message\n\n");
	} else {
		printf("The signature is INVALID for the provided message\n\n");
	}
}

/**
 * 
 */
void calculate_tag(uint8_t *mac_bytes, uint16_t mac_len, 
		const unsigned char *key, unsigned char *tag, unsigned char tag_len) {
	
	
	
    CCM_STAR.set_key(key);
	CCM_STAR.aead(nonce, mac_bytes, mac_len, a, a_len, tag, tag_len, forward);	
}


unsigned char generate_mac(unsigned char *payload, unsigned char *tag) {
	
	unsigned char l =14;
	
	memcpy(tag, "sics_validated", l);
	
	return l;
}


/********************** public functions ****************************/

void initiate_cose_hdr_struct(cose_header *cose_hdr, uint8_t protected) {
	
	cose_hdr->protected = protected;
	cose_hdr->map_len = 0;
}


int8_t add_cose_hdr_param(cose_header *cose_hdr, uint8_t label, uint8_t *value, uint8_t len) {
	
	if(cose_hdr->map_len >= MAX_COSE_HDR_ENTRY) {
		return 0;
	} else {
		cose_hdr->label[cose_hdr->map_len] = label;
		memcpy(cose_hdr->value[cose_hdr->map_len], value, len);
		cose_hdr->map_len++;
		return 1;
	}
	return 1;
}



/**
 * 
 */
void encrypt_decrypt_payload(uint8_t *payload_bytes, uint16_t payload_len) {
	
	//cose_hdr->value[1]
	CCM_STAR.set_key(COSE_SECRET);
	CCM_STAR.aead(nonce, payload_bytes, payload_len, a, a_len, result, mic_len, forward);
}


/**
 * 
 */
void calculate_mac_tag(uint8_t *hdr_bytes, uint8_t hdr_len, 
						uint8_t * payload_bytes, uint16_t payload_len) {

	/* setup mac data */
	uint16_t tobe_maced_len = hdr_len + payload_len;
	uint8_t *tobe_maced = (uint8_t *)malloc(tobe_maced_len*sizeof(uint8_t));
	
	memcpy(tobe_maced, hdr_bytes, hdr_len);
	memcpy(&tobe_maced[hdr_len], payload_bytes, payload_len);
	
	
	/* calulate the MAC */ 
	mic_len = 16;
	CCM_STAR.set_key(COSE_SECRET);
	CCM_STAR.aead(nonce, tobe_maced, tobe_maced_len, a, a_len, result, mic_len, forward);    
}

/**
 * encrypted cose message without any specific recipients
 * 
 */
void encode_cose_msg(cbor_data *data, cose_header *cose_hdr, enum cose_type ctype, 
						uint8_t *payload_bytes, uint16_t payload_len) {
	
	/* create array of 3 data items (protected header, unprotected header, 
	 * and payload */
	switch(ctype) {
		case COSE_ENCRYPT0:
			encode_data_array(data, 3);
			break;
		case COSE_SIGN1:
		case COSE_MAC0:
			encode_data_array(data, 4);
			break;
		default:
			printf("Unknown COSE type\n");
	}
	
	uint8_t *hdr_bytes = &data->buf[data->ptr];
	uint8_t hdr_len = (uint8_t) data->ptr;
	
	uint8_t result[32];
	
	/* encoded and add cose header */
	encode_cose_header(data, cose_hdr);
	
	/* calculate the encoded header length */
	hdr_len = (uint8_t) data->ptr - hdr_len;
	
	if(ctype == COSE_SIGN1) {
		calculate_signature(payload_bytes, payload_len, result);
	}
	else if(ctype == COSE_ENCRYPT0) {
		/* encrypt the payloads */
		encrypt_decrypt_payload(payload_bytes, payload_len);
	}
	else if(ctype == COSE_MAC0) {
		calculate_mac_tag(hdr_bytes, hdr_len, payload_bytes, payload_len);
	} 
	
	
	/* encode and add the payload (ciphertext) to the data buffer */
	encode_byte_string(data, payload_bytes, payload_len);
}

/**
 * decode encrypted cose message
 * 
 */
uint8_t decode_cose_msg(cbor_data *data, cose_header *cose_hdr, enum cose_type ctype, 
						uint8_t *payload_bytes) {
	
	uint8_t payload_len = 0;
	
	/* */
	decode_data_array(data);
	
	/* */
	decode_cose_header(data, cose_hdr);
	
	/* */
	payload_len = (uint8_t) decode_byte_string(data);
	if(payload_len != 0) {
		payload_bytes = &data->buf[data->ptr];
		
		if(ctype == COSE_SIGN1) {
			calculate_signature(payload_bytes, payload_len, result);
		}
		else if(ctype == COSE_ENCRYPT0) {
			/* decrypt the payloads */
			encrypt_decrypt_payload(payload_bytes, payload_len);
		}
		else if(ctype == COSE_MAC0) {
			//calculate_mac_tag(hdr_bytes, hdr_len, payload_bytes, payload_len);
		} 	
	}
	
	return payload_len;						
}

/**
 * encode major type 4: array of data items
 */
//unsigned short encode_cose_mac(unsigned char *bytes, unsigned short b, unsigned long len) {

/**
 * MAC-ed cose message with no specific recipient
 */
void encode_cose_mac0(cbor_data *data, cose_header *cose_hdr, uint8_t *payload_bytes, uint16_t payload_len) {
	
	/* create array of 4 data items (protected header, unprotected header, 
	 * payload, and MAC tag */
	encode_data_array(data, 4);
	
	uint8_t *hdr_bytes = &data->buf[data->ptr];
	uint8_t hdr_len = (uint8_t) data->ptr;
	
	/* encoded and add cose header */
	encode_cose_header(data, cose_hdr);
	
	/* calculate the encoded header length */
	hdr_len = (uint8_t) data->ptr - hdr_len;
	
	/* setup mac data */
	uint16_t mac_len = 4 + hdr_len + payload_len;
	uint8_t *mac_bytes = (uint8_t *)malloc(mac_len*sizeof(uint8_t));
	
	memcpy(mac_bytes, (uint8_t *)"mac0", 4);
	memcpy(&mac_bytes[4], hdr_bytes, hdr_len);
	memcpy(&mac_bytes[4+hdr_len], payload_bytes, payload_len);
	
	
	/* calulate the MAC */ 
	mic_len = 16;
    calculate_tag(mac_bytes, mac_len, COSE_SECRET, result, mic_len); 
    
	
	/* encode and add the payload (ciphertext) to the data buffer */
	encode_byte_string(data, payload_bytes, payload_len);
	
	/* calulate the MAC */ 
	//mic_len = 16;
    //calculate_tag(cose_hdr, payload_bytes, payload_len, COSE_SECRET, result, mic_len); 
    //CCM_STAR.set_key(COSE_SECRET);
    //mic_len = 16;
	//CCM_STAR.aead(nonce, payload_bytes, payload_len, a, a_len, result, mic_len, forward);
	
	/* add the MAC to the data buffer */
	encode_byte_string(data, result, mic_len);
}


/**
 * decode MAC-ed cose message
 */
uint8_t decode_cose_mac0(cbor_data *data, cose_header *cose_hdr, uint8_t *payload_bytes) {
	
	uint8_t len = 0;
	
	/* */
	len = (uint8_t) decode_data_array(data);
	
	/* */
	decode_cose_header(data, cose_hdr);
	
	/* */
	len = (uint8_t) decode_byte_string(data);
	if(len != 0) {
		payload_bytes = &data->buf[data->ptr];
		
		//CCM_STAR.set_key(COSE_SECRET);
		//CCM_STAR.aead(nonce, payload_bytes, len, a, a_len, result, mic_len, forward);
	}
	
	return len;
}

/**
 * 
 */
void varify_cose_payload(cbor_data *data, cose_header *cose_hdr, uint8_t *payload_bytes) {
	 
	uint8_t len = 0;
	
	/* */
	len = (uint8_t) decode_data_array(data);
	
	/* */
	decode_cose_header(data, cose_hdr);
	
	/* */
	len = (uint8_t) decode_byte_string(data);
	if(len != 0) {
		payload_bytes = &data->buf[data->ptr];
		
		CCM_STAR.set_key(cose_hdr->value[1]);
		CCM_STAR.aead(nonce, payload_bytes, len, a, a_len, result, mic_len, forward);
	}
}
