import base64
import argparse

def convert_cert_to_hex(cert_string):
    cert_string = str(cert_string.decode('base64').encode('hex'))
    cert_string = cert_string.upper()
    cert_string = ', 0x'.join(cert_string[i:i+2] for i in xrange(0,len(cert_string),2)) 
    return "{0x" + cert_string + "}"

def file_to_string(filename):
    with open(filename,"r") as myfile:
        data=myfile.read().replace('\n','')    
    return data

def remove_pem_from_string(pemstring):
    return pemstring.replace('-----BEGIN CERTIFICATE-----','').replace('-----BEGIN PUBLIC KEY-----','').replace('-----END PUBLIC KEY-----','').replace('-----END CERTIFICATE-----','').replace('-----BEGIN EC PRIVATE KEY-----','').replace('-----END EC PRIVATE KEY-----','') 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Converts a PEM encoded certificate file to a hex array.')
    parser.add_argument('cert', metavar='filename', type=str, nargs=1,
                   help='The name of the certificate file')

    args = parser.parse_args()
    cert_string = file_to_string(args.cert[0])
    cert_string = remove_pem_from_string(cert_string)
    print convert_cert_to_hex(cert_string)
