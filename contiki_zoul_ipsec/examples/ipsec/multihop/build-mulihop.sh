#!/bin/bash
#make TARGET=openmote clean all

#MAX_PAYLOAD=32
IPSEC=1
NODES=0

make clean udp-client TARGET=openmote WITH_COMPOWER=1 CONTIKI_WITH_IPSEC=$IPSEC MAX_PAYLOAD_LEN=$MAX_PAYLOAD NR_I_NODES=$NODES
sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB1 -e -w -v udp-client.bin
make clean udp-server TARGET=openmote CONTIKI_WITH_IPSEC=$IPSEC MAX_PAYLOAD_LEN=$MAX_PAYLOAD NR_I_NODES=$NODES
sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB0 -e -w -v udp-server.bin
if [ $NODES -eq 1 ];
then
	make clean intermediate-node TARGET=openmote WITH_COMPOWER=1 NR_I_NODES=$NODES NODE_ID=3 CONTIKI_WITH_IPSEC=0
	sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB2 -e -w -v intermediate-node.bin
	sudo putty -load Server & sudo putty -load Client & sudo putty -load Node3
elif [ $NODES -eq 2 ];
then
        make clean intermediate-node TARGET=openmote WITH_COMPOWER=1 NR_I_NODES=$NODES NODE_ID=3 CONTIKI_WITH_IPSEC=0
        sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB2 -e -w -v intermediate-node.bin
        make clean intermediate-node TARGET=openmote WITH_COMPOWER=1 NR_I_NODES=$NODES NODE_ID=4 CONTIKI_WITH_IPSEC=0
        sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB3 -e -w -v intermediate-node.bin
        sudo putty -load Server & sudo putty -load Client & sudo putty -load Node3 & sudo putty -load Node4
elif [ $NODES -eq 3 ];
then
        make clean intermediate-node TARGET=openmote WITH_COMPOWER=1 NR_I_NODES=$NODES NODE_ID=3 CONTIKI_WITH_IPSEC=0
        sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB2 -e -w -v intermediate-node.bin
        make clean intermediate-node TARGET=openmote WITH_COMPOWER=1 NR_I_NODES=$NODES NODE_ID=4 CONTIKI_WITH_IPSEC=0
        sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB3 -e -w -v intermediate-node.bin
        make clean intermediate-node TARGET=openmote WITH_COMPOWER=1 NR_I_NODES=$NODES NODE_ID=5 CONTIKI_WITH_IPSEC=0
        sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB4 -e -w -b 50000 -v intermediate-node.bin
        sudo putty -load Server & sudo putty -load Client & sudo putty -load Node3 & sudo putty -load Node4 & sudo putty -load Node5
else
sudo putty -load Server & sudo putty -load Client
fi
