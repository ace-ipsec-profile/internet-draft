/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/**
 * Modified UDP-client that uses static routing
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "clock.h"
#include "sys/rtimer.h"

#if WITH_IPSEC
#include "machine.h"
#include "sad.h"
#include "ike.h"
#endif

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#if WITH_COMPOWER
#include "powertrace.h"
#endif

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define SETUP_INTERVAL  20 * CLOCK_SECOND
#define SEND_INTERVAL 10 * CLOCK_SECOND

#ifndef MAX_PAYLOAD_LEN
#define MAX_PAYLOAD_LEN   512
#endif

/* Number of intermediate nodes, set in ipsec.conf*/
#ifndef NR_NODES
#define NR_NODES 0
#endif

/* Number of packets to send */
#ifndef NR_PACKETS
#define NR_PACKETS 100
#endif

uint8_t nr_nodes = NR_NODES;
uint8_t nr_packets = NR_PACKETS;

static struct uip_udp_conn *client_conn;
static uip_ipaddr_t server_ipaddr;
static uint32_t start_times[NR_PACKETS];
static uint32_t end_times[NR_PACKETS];

/*---------------------------------------------------------------------------*/
PROCESS(udp_client_process, "UDP client process");
#if WITH_OPENMOTE
#include "flash-erase.h"
AUTOSTART_PROCESSES(&udp_client_process, &flash_erase_process);
#else
AUTOSTART_PROCESSES(&udp_client_process);
#endif
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(uint8_t *count)
{
  char *str;
  char number[3];
  memset(number, 0, 3);

  if(uip_newdata()) {
    str = uip_appdata;
    str[uip_datalen()] = '\0';
    PRINTF("Response from the server: '%s'\n", str);

    uint8_t length = strlen(str);
    if(length == 26) {
      number[2] = str[24];
      *count = (uint8_t)atoi(&number[2]);
    } else if(length == 27) {
      number[2] = str[25];
      number[1] = str[24];
      *count = (uint8_t)atoi(&number[1]);
    } else if(length == 28) {
      number[2] = str[26];
      number[1] = str[25];
      number[0] = str[24];
      *count = (uint8_t)atoi(&number[0]);
    }
  }
}
/*---------------------------------------------------------------------------*/
static char buf[MAX_PAYLOAD_LEN];
static void
timeout_handler(uint8_t count, uint16_t size)
{
  static int seq_id;

  PRINTF("Client sending to: ");
  PRINT6ADDR(&client_conn->ripaddr);
  sprintf(buf, "Hello %d from the client", count);
  PRINTF(" (msg: %s)\n", buf);
#if SEND_TOO_LARGE_PACKET_TO_TEST_FRAGMENTATION
  uip_udp_packet_send(client_conn, buf, UIP_APPDATA_SIZE);
#else /* SEND_TOO_LARGE_PACKET_TO_TEST_FRAGMENTATION */
  uip_udp_packet_send(client_conn, buf, size);
#endif /* SEND_TOO_LARGE_PACKET_TO_TEST_FRAGMENTATION */
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Client IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
#if UIP_CONF_ROUTER
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

#endif /* UIP_CONF_ROUTER */
}
static void
add_route(void)
{
  /* Set the addresses of cooja nodes*/
  /* Client <-> 1st <-> 2nd <-> 3rd <-> Server */
  uip_ipaddr_t ipaddr_link_server, ipaddr_global_server,
               ipaddr_link_node3, ipaddr_server_next_hop;

#if WITH_COOJA
  /* Server global address*/
  uip_ip6addr(&ipaddr_global_server, 0xaaaa, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);

  /* Set Link-Local addresses */
  uip_ip6addr(&ipaddr_link_server, 0xfe80, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);
  uip_ip6addr(&ipaddr_link_node3, 0xfe80, 0, 0, 0, 0x0203, 0x0003, 0x0003, 0x0003);

  /* Set Link-Layer addresses */
  uip_lladdr_t next_hop1 = { { 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01 } }; /* Server */
  uip_lladdr_t next_hop3 = { { 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03 } }; /* 1st Intermediate-node */
#else
  /* Set the addresses of real nodes*/
  /* Server global address*/
  uip_ip6addr(&ipaddr_global_server, 0xaaaa, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b2b);

  /* Set Link-Local addresses */
  uip_ip6addr(&ipaddr_link_server, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b2b);
  uip_ip6addr(&ipaddr_link_node3, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b09);

  /* Set Link-Layer addresses */
  uip_lladdr_t next_hop1 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x2b } }; /* Server */
  uip_lladdr_t next_hop3 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x09 } }; /* 1st Intermediate-node */
#endif
  switch(nr_nodes) {
  case 0:
    uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_server);
    uip_ds6_nbr_add(&ipaddr_server_next_hop, &next_hop1, 0, NBR_STALE);
    break;
  case 1:
  case 2:
  case 3:
    uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_node3);
    uip_ds6_nbr_add(&ipaddr_server_next_hop, &next_hop3, 0, NBR_STALE);
    break;
  }

  uip_ds6_route_add(&ipaddr_global_server, 128, &ipaddr_server_next_hop);
}
static void
print_RTT(uint32_t *start, uint32_t *end, uint8_t count)
{
  printf("Round Trip Times\n");
  int i;
  uint32_t RTT;

  for(i = 0; i < count; i++) {
    RTT = (end[i] == 0) ? 0 : end[i] - start[i];

    printf("%lu, ", RTT);
  }
  printf("\n");
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_client_process, ev, data)
{
  static struct etimer et;
  static uint8_t count = 0;

  /* RTT timers*/
  static rtimer_clock_t start_time;

  static uint8_t start_timers;

#if WITH_COMPOWER
  static int print = 0;
#endif

  PROCESS_BEGIN();

  PROCESS_PAUSE();

#if UIP_CONF_ROUTER
  set_global_address();
#endif

#if WITH_COOJA
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);
#else
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b2b);
#endif

  memset(start_times, 0, sizeof(start_times));
  memset(end_times, 0, sizeof(start_times));
  memset(buf, 1, MAX_PAYLOAD_LEN);

  printf("UDP client process started\n");
  printf("Number of Intermediate nodes %u\n", nr_nodes);

  add_route();

  print_local_addresses();

  /* new connection with remote host */
  client_conn = udp_new(&server_ipaddr, UIP_HTONS(3000), NULL);
  udp_bind(client_conn, UIP_HTONS(3001));

  PRINTF("Created a connection with the server ");
  PRINT6ADDR(&client_conn->ripaddr);
  PRINTF(" local/remote port %u/%u\n",
         UIP_HTONS(client_conn->lport), UIP_HTONS(client_conn->rport));

#if WITH_COMPOWER
  powertrace_sniff(POWERTRACE_ON);
#endif

#if WITH_IPSEC
  etimer_set(&et, SETUP_INTERVAL);
#else
  etimer_set(&et, SEND_INTERVAL);
  start_timers = 1;
#if WITH_COMPOWER
  powertrace_print("Start: ");
#endif
#endif
  static uint16_t size_paylaod = 32;

  while(1) {
    PROCESS_YIELD();
    if(etimer_expired(&et)) {
      if(count < NR_PACKETS && size_paylaod <= MAX_PAYLOAD_LEN) {
        if(start_timers) {
          start_time = RTIMER_NOW();
          start_times[count] = (uint32_t)((uint64_t)start_time * 1000000 / RTIMER_SECOND);
          count = count + 1;
          if(count == NR_PACKETS) {
            /* Set timer to wait for last responses */
            etimer_set(&et, SETUP_INTERVAL);
            etimer_restart(&et);
          }
        }
        timeout_handler(count, size_paylaod);
        etimer_restart(&et);
      } else {
        /* We have sent the required number of packets and print out the times */
#if WITH_COMPOWER
        powertrace_print("End: ");
#endif
        print_RTT(start_times, end_times, NR_PACKETS);
        etimer_restart(&et);
        etimer_set(&et, SEND_INTERVAL);
        memset(start_times, 0, sizeof(start_times));
        memset(end_times, 0, sizeof(start_times));
        count = 0;
        size_paylaod = size_paylaod * 2;
        if(size_paylaod > MAX_PAYLOAD_LEN) {
          etimer_stop(&et);
        }
#if WITH_COMPOWER
        powertrace_print("Start: ");
#endif
      }
#if WITH_IPSEC
    } else if(ev == ike_negotiate_done) {
      /* Start the timers when we recieve a Negotiation done event*/
      printf("IKE Negotiation done event\n");
      etimer_set(&et, SEND_INTERVAL);
      etimer_restart(&et);
#if WITH_COMPOWER
      powertrace_print("Start: ");
#endif
      start_timers = 1;
#endif
    } else if(ev == tcpip_event) {
      /* Process responses from the server */
      uint8_t j = 0;

      tcpip_handler(&j);

      if(j != 0 && j <= NR_PACKETS) {
        start_time = RTIMER_NOW();
        end_times[j - 1] = (uint32_t)((uint64_t)start_time * 1000000 / RTIMER_SECOND);
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
