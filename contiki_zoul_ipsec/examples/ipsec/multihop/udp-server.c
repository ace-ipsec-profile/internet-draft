/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/**
 * Modified UDP server that uses static routing and sends UDP packets of
 * different sizes
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/rpl/rpl.h"
#include "net/netstack.h"
#include "spd.h"
#include <string.h>
#include <stdlib.h>

#ifndef NR_NODES
#define NR_NODES 0
#endif
uint8_t nr_nodes = NR_NODES;

#if WITH_COMPOWER
#include "powertrace.h"
#endif

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

PROCESS(udp_server_process, "UDP server process");

#if WITH_OPENMOTE
#include "flash-erase.h"
AUTOSTART_PROCESSES(&udp_server_process, &flash_erase_process);
#else
AUTOSTART_PROCESSES(&udp_server_process);
#endif

static struct uip_udp_conn *server_conn;

/*---------------------------------------------------------------------------*/
static char buf[1024];
static void
tcpip_handler(void)
{
  static int seq_id;
  /* char buf[MAX_PAYLOAD_LEN]; */
  char *str;
  char number[3];
  memset(number, 0, 3);
  uint16_t send_back_len = 0;

  if(uip_newdata()) {
    str = uip_appdata;
    send_back_len = uip_datalen();

    str[uip_datalen()] = '\0';
    PRINTF("Server received: '%s' from ", str);
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF("\n");

    switch(strlen(str)) {
    case 23:
      number[2] = str[6];
      seq_id = atoi(&number[2]);
      break;
    case 24:
      number[2] = str[7];
      number[1] = str[6];
      seq_id = atoi(&number[1]);
      break;
    case 25:
      number[2] = str[8];
      number[1] = str[7];
      number[0] = str[6];
      seq_id = atoi(&number[0]);
      break;
    }
    uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
    PRINTF("Responding with message: ");
    sprintf(buf, "Hello from the server! (%d)", seq_id);
    PRINTF("%s\n", buf);
    printf("%u:%u\n", send_back_len, seq_id);
    uip_udp_packet_send(server_conn, buf, send_back_len);
    /* Restore server connection to allow data from any node */
    memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
  }
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
}
static void
add_route(void)
{
  /* Client <-> 1st <-> 2nd <-> 3rd <-> Server */
  uip_ipaddr_t ipaddr_link_client, ipaddr_global_client,
               ipaddr_link_node3, ipaddr_link_node4, ipaddr_link_node5,
               ipaddr_client_next_hop;

#if WITH_COOJA
  /* Cooja nodes */
  /* Set the global addresses of the server and client */
  uip_ip6addr(&ipaddr_global_client, 0xaaaa, 0, 0, 0, 0x0202, 0x0002, 0x0002, 0x0002);

  /* Set link local addresses */
  uip_ip6addr(&ipaddr_link_client, 0xfe80, 0, 0, 0, 0x0202, 0x0002, 0x0002, 0x0002);
  uip_ip6addr(&ipaddr_link_node3, 0xfe80, 0, 0, 0, 0x0203, 0x0003, 0x0003, 0x0003);
  uip_ip6addr(&ipaddr_link_node4, 0xfe80, 0, 0, 0, 0x0204, 0x0004, 0x0004, 0x0004);
  uip_ip6addr(&ipaddr_link_node5, 0xfe80, 0, 0, 0, 0x0205, 0x0005, 0x0005, 0x0005);

  /* Set the link-layer addresses */
  uip_lladdr_t next_hop2 = { { 0x00, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00, 0x02 } }; /* Client */
  uip_lladdr_t next_hop3 = { { 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03 } }; /* 1st Intermediate-node */
  uip_lladdr_t next_hop4 = { { 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04 } }; /* 2nd Intermediate-node */
  uip_lladdr_t next_hop5 = { { 0x00, 0x05, 0x00, 0x05, 0x00, 0x05, 0x00, 0x05 } }; /* 3rd Intermediate-node */
#else
  /* Real nodes */
  /* Set the global addresses of the server and client */
  uip_ip6addr(&ipaddr_global_client, 0xaaaa, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b19);

  /* Set link local addresses */
  uip_ip6addr(&ipaddr_link_client, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b19);
  uip_ip6addr(&ipaddr_link_node3, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b09);
  uip_ip6addr(&ipaddr_link_node4, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b13);
  uip_ip6addr(&ipaddr_link_node5, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9af8);

  /* Set the link-layer addresses */
  uip_lladdr_t next_hop2 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x19 } }; /* Client */
  uip_lladdr_t next_hop3 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x09 } }; /* 1st Intermediate-node */
  uip_lladdr_t next_hop4 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x13 } }; /* 2nd Intermediate-node */
  uip_lladdr_t next_hop5 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9a, 0xf8 } }; /* 3rd Intermediate-node */
#endif

  switch(nr_nodes) {
  case 0:
    uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_client);
    uip_ds6_nbr_add(&ipaddr_client_next_hop, &next_hop2, 0, NBR_STALE);
    break;
  case 1:
    uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_node3);
    uip_ds6_nbr_add(&ipaddr_client_next_hop, &next_hop3, 0, NBR_STALE);
    break;
  case 2:
    uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_node4);
    uip_ds6_nbr_add(&ipaddr_client_next_hop, &next_hop4, 0, NBR_STALE);
    break;
  case 3:
    uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_node5);
    uip_ds6_nbr_add(&ipaddr_client_next_hop, &next_hop5, 0, NBR_STALE);
    break;
  }

  uip_ds6_route_add(&ipaddr_global_client, 128, &ipaddr_client_next_hop);
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_server_process, ev, data)
{
#if UIP_CONF_ROUTER
  uip_ipaddr_t ipaddr;
#endif /* UIP_CONF_ROUTER */
  struct uip_ds6_addr *root_if;

#if WITH_COMPOWER
  static int print = 0;
#endif

  PROCESS_BEGIN();

  PROCESS_PAUSE();
  memset(buf, 1, 1024);
  printf("UDP server started\n");
  printf("Number of Intermediate nodes %u\n", nr_nodes);

  set_global_address();

  print_local_addresses();

  add_route();

  server_conn = udp_new(NULL, UIP_HTONS(3001), NULL);
  udp_bind(server_conn, UIP_HTONS(3000));

#if WITH_COMPOWER
  powertrace_sniff(POWERTRACE_ON);
#endif

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
#if WITH_COMPOWER
      if(print == 0) {
        powertrace_print("#P UDP");
      }
      if(++print == 3) {
        print = 0;
      }
#endif
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
