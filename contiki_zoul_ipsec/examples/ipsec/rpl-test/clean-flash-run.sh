#!/bin/bash
#make TARGET=openmote clean all
make clean rpl-key-client TARGET=openmote CLIENT_CONF=1
sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB1 -e -w -v rpl-key-client.bin
make clean rpl-key-server TARGET=openmote SERVER_CONF=1
sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB0 -e -w -v rpl-key-server.bin
sudo putty -load Contiki & sudo putty -load Contiki2
