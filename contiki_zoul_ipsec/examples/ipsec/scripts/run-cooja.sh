cd ../../ipv6/rpl-border-router/ && (make TARGET=wismote)
cd ../../ipsec && (make TARGET=wismote WITH_COMPOWER=1)
sudo ./scripts/strongswan/reset_ike_ipsec.sh &&
sudo wireshark -i any -f "host aaaa::1" -k &
gnome-terminal -e "bash -c \"make TARGET=cooja ipsec-example-powertrace.csc; exec bash\"" &
sleep 10 && (cd ../ipv6/rpl-border-router/ && (make TARGET=cooja connect-router-cooja))

