Update to strongswan 5.3.0 on Ubuntu 12.04
Install required dependencies:
sudo apt-get install gcc    	# installs the C++ compiler.
sudo apt-get install make   	# installs the make program to build code.
sudo apt-get install m4     	# 
sudo apt-get install libssl-dev # for ssl libcrpyto required by ECC

Download and install GMP library required by Strongswan
mkdir gmp_src
cd gmp_src
wget ftp://ftp.gmplib.org/pub/gmp-5.1.3/gmp-5.1.3.tar.lz
sudo apt-get install lzip
lzip --decompress *.lz
tar xf *.tar
cd gmp-5.1.3
sudo ./configure        # this checks for dependencies and other things.
                       	# Pay attention to error output that shows up at the end of ./configure.
sudo make               # build. If errors, try running 2nd time. I've seen this happen.
sudo make check         # some other extra check.
sudo make install       # installs the library.
cd ../..


Download and install strongswan 5.3.0
mkdir strongswan_src
pushd strongswan_src
wget http://download.strongswan.org/strongswan-5.3.0.tar.gz
tar xvf strongswan-5.3.0.tar.gz
cd strongswan-5.3.0

Configure strongswan with OpenSSL, AES-CCM and AES-CTR
sudo ./configure --prefix=/usr --sysconfdir=/etc --enable-openssl --enable-ccm --enable-ctr 
sudo make                      # build.
sudo make install              # installs everything.
cd ../..

Run sudo ipsec restart and the version should be 5.3.0
sudo ipsec restart
Stopping strongSwan IPsec...
Starting strongSwan 5.3.0 IPsec [starter]...

Run sudo ipsec listalgs to see what transforms are supported should contain ECP_256, AES_CTR, AES_CCM
List of registered IKE algorithms:

  encryption: AES_CBC[aes] 3DES_CBC[des] DES_CBC[des] DES_ECB[des] RC2_CBC[rc2] CAMELLIA_CBC[openssl] CAST_CBC[openssl]
              BLOWFISH_CBC[openssl] NULL[openssl] AES_CTR[ctr] CAMELLIA_CTR[ctr]
  integrity:  HMAC_MD5_96[openssl] HMAC_MD5_128[openssl] HMAC_SHA1_96[openssl] HMAC_SHA1_128[openssl]
              HMAC_SHA1_160[openssl] HMAC_SHA2_256_128[openssl] HMAC_SHA2_256_256[openssl] HMAC_SHA2_384_192[openssl]
              HMAC_SHA2_384_384[openssl] HMAC_SHA2_512_256[openssl] HMAC_SHA2_512_512[openssl] CAMELLIA_XCBC_96[xcbc]
              AES_XCBC_96[xcbc] AES_CMAC_96[cmac]
  aead:       AES_GCM_8[openssl] AES_GCM_12[openssl] AES_GCM_16[openssl] AES_CCM_8[ccm] AES_CCM_12[ccm] AES_CCM_16[ccm]
              CAMELLIA_CCM_8[ccm] CAMELLIA_CCM_12[ccm] CAMELLIA_CCM_16[ccm]
  hasher:     HASH_SHA1[sha1] HASH_SHA224[sha2] HASH_SHA256[sha2] HASH_SHA384[sha2] HASH_SHA512[sha2] HASH_MD5[md5]
              HASH_MD4[openssl]
  prf:        PRF_KEYED_SHA1[sha1] PRF_HMAC_MD5[openssl] PRF_HMAC_SHA1[openssl] PRF_HMAC_SHA2_256[openssl]
              PRF_HMAC_SHA2_384[openssl] PRF_HMAC_SHA2_512[openssl] PRF_FIPS_SHA1_160[fips-prf] PRF_AES128_XCBC[xcbc]
              PRF_CAMELLIA128_XCBC[xcbc] PRF_AES128_CMAC[cmac]
  dh-group:   MODP_2048[openssl] MODP_2048_224[openssl] MODP_2048_256[openssl] MODP_1536[openssl] MODP_3072[openssl]
              MODP_4096[openssl] MODP_6144[openssl] MODP_8192[openssl] MODP_1024[openssl] MODP_1024_160[openssl]
              MODP_768[openssl] MODP_CUSTOM[openssl] ECP_256[openssl] ECP_384[openssl] ECP_521[openssl] ECP_224[openssl]
              ECP_192[openssl] ECP_224_BP[openssl] ECP_256_BP[openssl] ECP_384_BP[openssl] ECP_512_BP[openssl]
  random-gen: RNG_WEAK[openssl] RNG_STRONG[random] RNG_TRUE[random]
  nonce-gen:  [nonce]






