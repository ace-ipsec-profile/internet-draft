#ifndef __IPSEC_PROJECT_H__
#define __IPSEC_PROJECT_H__

#define UIP_CONF_ROUTER 1

/* CC2538 config */
#ifndef ENERGEST_CONF_ON
#define ENERGEST_CONF_ON            1 /**< Energest Module */
#endif

#ifndef LPM_CONF_ENABLE
#define LPM_CONF_ENABLE       1 /**< Set to 0 to disable LPM entirely */
#endif

#ifndef LPM_CONF_MAX_PM
#define LPM_CONF_MAX_PM       1 /* was 1 */
#endif

#ifndef LPM_CONF_STATS
#define LPM_CONF_STATS        1 /**< Set to 1 to enable LPM-related stats */
#endif

#undef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC              nullrdc_driver

//#undef NETSTACK_CONF_MAC
//#define NETSTACK_CONF_MAC              nullmac_driver


/* Extra uIP logging */
#undef UIP_CONF_LOGGING
#define UIP_CONF_LOGGING 0

#ifdef UIP_CONF_BUFFER_SIZE
#undef UIP_CONF_BUFFER_SIZE
#endif
#define UIP_CONF_BUFFER_SIZE  1280 /* This option can be set in various platform specific header files as well */

#define IKE_SAD_ENTRIES 5

#define IKE_SESSION_NUM 5

#define WATCHDOG_CONF_ENABLE 0 /**< Disable the watchdog timer */

/* Enable IPSEC debugging */
#define IPSEC_DEBUG 0
#define DEBUG 0
#define IKE_IPSEC_INFO 0
#define IPSEC_TIME_STATS 0

#define CONTIKI_WITH_IPV6       1
#define NETSTACK_CONF_WITH_IPV6 1

/* IPsec configuration ------------------*/
/* Enabling ESP is equal to enabling to IPsec. Note that AH is not supported! */
#define WITH_CONF_IPSEC_ESP             1

/* The IKE subsystem is optional if the SAs are manually configured */
#define WITH_CONF_IPSEC_IKE             1

/*
 * Manual SA configuration allows you as developer to create persistent SAs in the SAD.
 * This is probably what you want to use if WITH_CONF_IPSEC_IKE is set 0, but please note
 * that both features can be used simultaneously on a host as per the IPsec RFC.
 *
 * The manual SAs can be set in the function sad_conf()
 */
#define WITH_CONF_MANUAL_SA   0

/*
 * Enabling certificate authentication for the IKE AUTH payload
 * If the responder uses certificate authentication
 */

/* Shared secret to use when not using certificate authentication */
/* Shared secret defined below used in the IKE_AUTH payload */
#define SHARED_IKE_SECRET "aa280649dc17aa821ac305b5eb09d445"

/* ID up to 16 bytes*/
#define IKE_ID "test@sics.se"

/* IKE SA Configuration */
/* Integrity transform to use for the IKE SA, supported transforms:
   SA_INTEG_AES_XCBC_MAC_96, SA_INTEG_HMAC_SHA1_96, SA_INTEG_HMAC_SHA2_256_128
   NOTE: don't define if IKE_ENCR = SA_ENCR_CCM_{8,12,16}*/
//#define IKE_INTEG SA_INTEG_HMAC_SHA2_256_128

/* Encryption transform to use for the IKE SA, supported transforms:
   SA_ENCR_AES_CTR, SA_ENCR_AES_CCM_8, SA_ENCR_AES_CCM_12, SA_ENCR_AES_CCM_16 */
#define IKE_ENCR SA_ENCR_AES_CCM_8

/* Psuedo-random function to use for the IKE SA, supported functions:
   SA_PRF_HMAC_SHA1, SA_PRF_HMAC_SHA2_256*/
#define IKE_PRF SA_PRF_HMAC_SHA2_256

/* Diffie-Hellman groups supported for IKE:
   SA_DH_192_RND_ECP_GROUP, SA_DH_256_RND_ECP_GROUP, SA_DH_256_BP_GROUP
   NOTE: When certificate authentication is used this defaults to
   SA_DH_256_RND_ECP_GROUP also this parameter must match CURVE_PARAMS from
   the makefile, that is
   SA_DH_192_RND_ECP_GROUP => CURVE_PARAMS = SECP192R1
   SA_DH_256_RND_ECP_GROUP => CURVE_PARAMS = SECP256R1 (Only supported ECDSA algorithm)
   SA_DH_256_BP_GROUP      => CURVE_PARAMS = BPOOLP256R1
 */
#define IKE_DH SA_DH_256_RND_ECP_GROUP

/* ESP SA Configuration */
/* Supported transforms, same as for IKE SA*/
#define ESP_ENCR  SA_ENCR_AES_CCM_8
/* Supported transforms, same as for IKE SA
   NOTE: don't define if ESP_ENCR=SA_ENCR_AES_CCM_{8,12,16}*/
/* #define ESP_INTEG SA_INTEG_HMAC_SHA2_256_128 */

/**
 * Configuring an AES implementation
 *
 * The only current implementation is that provided by the MIRACL -library. In the future
 * this can be extended with an interface to the CC2420 radio module which is equipped with
 * an AES hardware implementation.
 */
#ifdef HW_AES
#define CRYPTO_CONF_AES cc2538_aes
#else
#define CRYPTO_CONF_AES contiki_aes
#endif
/* ECC */
#define WORDS_32_BITS 1
// #define STATIC_ECC_KEY
#define CC2538_RF_CONF_CHANNEL 18
#define QUEUEBUF_CONF_NUM 12

#endif
