/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Erbium (Er) REST Engine example.
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdio.h>
#include <stdlib.h>
// #include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "rest-engine.h"
// #include "net/ip/uip-debug.h"

// #define NDEBUG
// #define CONTIKIMAC_CONF_COMPOWER         0
// #define XMAC_CONF_COMPOWER               0
// #define CXMAC_CONF_COMPOWER              0
// #define ENERGEST_CONF_ON 0
// #define PROCESS_CONF_NO_PROCESS_NAMES   1
// #define UIP_CONF_RPL                    0  <- set in Makefile by developers
// #define UIP_CONF_IPV6_RPL               0  <- added by me!




// #if PLATFORM_HAS_BUTTON
// #include "dev/button-sensor.h"
// #endif


/*----------------------------------------------------------------------------*/
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:" \
                                "%02x%02x:%02x%02x:%02x%02x:%02x%02x]", \
                                ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], \
                                ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], \
                                ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], \
                                ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], \
                                ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], \
                                ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], \
                                ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], \
                                ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTFLN(...)
#endif

/*----------------------------------------------------------------------------*/

//============================================================================
//=============================IPsec    ======================================
//============================================================================
#define MOTE_PORT 1234
#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define UIP_UDP_BUF  ((struct uip_udp_hdr *)&uip_buf[uip_l2_l3_hdr_len])
static struct uip_udp_conn *server_conn;


//============================================================================
/*
 * Resources to be activated need to be imported through the extern keyword.
 * The build system automatically compiles the resources in the corresponding sub-directory.
 */
extern resource_t
  res_ace_token;

// #include "net/ip/uip.h"
// #include "net/ipv6/uip-ds6.h"
// #include "net/ip/uip-udp-packet.h"
#include "sad.h"


// static void
// print_local_addresses(void)
// {
//   int i;
//   uint8_t state;

//   PRINTF("RS IPv6 addresses: ");
//   for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
//     state = uip_ds6_if.addr_list[i].state;
//     if(uip_ds6_if.addr_list[i].isused &&
//        (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
//       PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
//       PRINTF("\n");
//       /* hack to make address "final" */
//       if (state == ADDR_TENTATIVE) {
//   uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
//       }
//     }
//   }
// }

static void
tcpip_handler(void)
{
  char *data = uip_appdata;
  uint16_t datalen = uip_datalen();

  if(uip_newdata()) {
    int i = 0;

    uip_len = 0;

    /* PRINTF("IPSEC-EXAMPLE before: %u", UIP_HTONS(server_conn->rport)); */

    uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
    udp_bind(server_conn, UIP_HTONS(MOTE_PORT));
    server_conn->rport = UIP_UDP_BUF->srcport;

    PRINTF("Replied:\"");
    for(i = 0; i < (datalen-1); i++) {
      printf("%c", ++data[i]);
    }
    PRINTF("\"\n(length %u)\n", datalen);

    // uint32_t cpu = energest_type_time(ENERGEST_TYPE_CPU);
    // uint32_t transmit = energest_type_time(ENERGEST_TYPE_TRANSMIT);

    uip_udp_packet_send(server_conn, data, datalen);

    // cpu = energest_type_time(ENERGEST_TYPE_CPU) - cpu;
    // transmit = energest_type_time(ENERGEST_TYPE_TRANSMIT) - transmit;

    // uint32_t arch_second = RTIMER_ARCH_SECOND;
    // PRINTF("CPU time: %u, TRANSMIT time: %u, arch second %u\n", cpu, transmit, arch_second);

    memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
    server_conn->rport = 0;
  }
}

PROCESS(res_server, "Resource Server");
AUTOSTART_PROCESSES(&res_server);

PROCESS_THREAD(res_server, ev, data)
{
  PROCESS_BEGIN();

  PROCESS_PAUSE();

  PRINTF("Starting RS\n");
  // print_local_addresses();

  PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
  PRINTF("LL header: %u\n", UIP_LLH_LEN);
  PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
  PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

  /* Initialize the REST engine. */
  rest_init_engine();
//============================================================================
//=============================IPsec    ======================================
//============================================================================
#define MOTE_PORT 1234
  // PRINTF("ipsec-example: calling udp_new\n");
  server_conn = udp_new(NULL, UIP_HTONS(0), NULL);
  udp_bind(server_conn, UIP_HTONS(MOTE_PORT));



//============================================================================
  /*
   * Bind the resources to their Uri-Path.
   * WARNING: Activating twice only means alternate path, not two instances!
   * All static variables are the same for each URI path.
   */
 // rest_activate_resource(&res_hello, ".well-known/core");

  rest_activate_resource(&res_ace_token, "token");

    static uip_ip6addr_t ip_other;
    static uip_ip6addr_t ip_me;
    const uint8_t encr_key[] =
    // {
    //   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xa1, 0x00, 0x00,0x00, 0x00, 0x07, 0x59, 0x0
    // };
    //
    {
      0x3b, 0xda, 0x5b, 0x6c, 0x05, 0x59, 0x5d, 0xe5, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xd1, 0xaf, 0xd4,0xd4, 0xa8, 0x07, 0x59
    };
    uip_ip6addr(&ip_me,    0xaaaa, 0, 0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
    uip_ip6addr(&ip_other, 0xaaaa, 0, 0, 0x0, 0x0, 0x0, 0x0, 0x1);
    printf("Initializing SPD...\n");
    sad_set(
        &ip_other,
        &ip_me,
        &encr_key,
        sizeof(encr_key),
        2,
        1,// 2,
        SA_PROTO_ESP,//new_ipsec_sa.prot_type, // SA_PROTO_ESP,
        SA_ENCR_AES_CTR);//new_ipsec_sa.encr_alg);// SA_ENCR_AES_CTR);
printf("Setting SA. DONE :D\n");

  /* Define application-specific events here. */
  while(1) {
    PROCESS_WAIT_EVENT();
    printf("processing tcp");
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }                             /* while (1) */

  PROCESS_END();
}
