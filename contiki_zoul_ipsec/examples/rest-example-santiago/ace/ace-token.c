
// #include "cbor-encoder.h"
// #include "cbor-web-token.h"
// #include "cose-encoder.h"

#include "contiki.h"
#include "ace-token.h"
// #include "cbor-web-token.h"

uint16_t generate_cose_key( uint8_t *payload_bytes,
	                       char *cose_key_type,
	                       char *cose_key_id,
	                       char *key) {

	 cbor_data payload;
	 payload.buf = payload_bytes;
	 payload.ptr = 0;
	 /* encode CBOR web token as a payload */
	 encode_map_array(&payload, 3);

	 encode_cose_key(&payload, COSE_KEY_TYPE, (uint8_t *) cose_key_type);
	 encode_cose_key(&payload, COSE_KEY_ID, (uint8_t *) cose_key_id);
	 encode_cose_key(&payload, KEY, (uint8_t *) key);
	 return payload.ptr;
}

uint16_t generate_ipsec(uint8_t *payload_bytes,
                       char *mode,
                       char *protocol,
                       uint32_t life,
                       char *ip_c,
                       char *ip_rs,
                       uint32_t spi_sa_c,
                       uint32_t spi_sa_rs,
                       uint32_t prot_type,
                       uint32_t enc_alg,
                       char *seed) {

	 cbor_data payload;
	 payload.buf = payload_bytes;
	 payload.ptr = 0;
	 uint8_t dstr[4];
	 /* encode CBOR web token as a payload */
	 encode_map_array(&payload, 10);

	 encode_ipsec(&payload, MODE, (uint8_t *) mode);
	 encode_ipsec(&payload, PROTOCOL, (uint8_t *) protocol);
	 integer2bytestring(life, 4, dstr);
	 encode_ipsec(&payload, LIFE, dstr);
	 encode_ipsec(&payload, IP_C, (uint8_t *) ip_c);
	 encode_ipsec(&payload, IP_RS, (uint8_t *)ip_rs);
	 integer2bytestring(spi_sa_c, 4, dstr);
	 encode_ipsec(&payload, SPI_SA_C, dstr);
	 integer2bytestring(spi_sa_rs, 4, dstr);
	 encode_ipsec(&payload, SPI_SA_RS, dstr);
	 encode_ipsec(&payload, IPSEC_ENC_ALG, (uint8_t *) prot_type);
	 encode_ipsec(&payload, IPSEC_PROT_TYPE, (uint8_t *) enc_alg);
	 encode_ipsec(&payload, SEED, (uint8_t *) seed);
	 return payload.ptr;
}

uint16_t generate_cnf( uint8_t *payload_bytes,
						   uint32_t cose_len,
	                       char *cose_key,
	                       char *cose_encrypted,
	                       char *cnf_key_id,
	                       uint32_t ipsec_len,
	                       char *ipsec,
	                       char *kmp) {

	 cbor_data payload;
	 payload.buf = payload_bytes;
	 payload.ptr = 0;
	 /* encode CBOR web token as a payload */
	 encode_map_array(&payload, 5);

	 encode_cnf(&payload, COSE_ENCRYPTED, (uint8_t *) cose_encrypted);
	 encode_cnf(&payload, CNF_KEY_ID, (uint8_t *) cnf_key_id);
	 encode_cnf(&payload, KMP, (uint8_t *) kmp);
	 encode_struc_in_cnf(&payload, COSE_KEY, (uint8_t *) cose_key, cose_len );
	 encode_struc_in_cnf(&payload, IPSEC_STRUC, (uint8_t *) ipsec, ipsec_len);
	 return payload.ptr;
}


uint16_t generate_access_token( uint8_t *payload_bytes,
	                           char *subject,
	                           char *audience,
	                           uint32_t exp_time,
	                           char *client_id,
	                           char *scope,
	                           uint32_t grant_type,
	                           char *access_token,
	                           char *token_type,
	                           char *username,
	                           char *password,
	                           char *cnf,
	                           char *profile) {

	 cbor_data payload;
	 payload.buf = payload_bytes;
	 payload.ptr = 0;
	 uint8_t dstr[4];

	 /* encode CBOR web token as a payload */
	 encode_map_array(&payload, 12);

	 encode_cwt_directly(&payload, SUB, (uint8_t *) subject);
	 encode_cwt_directly(&payload, AUD, (uint8_t *) audience);
	 integer2bytestring(exp_time, 4, dstr);
	 encode_cwt_directly(&payload, EXP, dstr);
	 encode_cwt_directly(&payload, CLIENT_ID, (uint8_t *) client_id);
	 encode_cwt_directly(&payload, SCOPE, (uint8_t *) scope);
	 integer2bytestring(grant_type, 4, dstr);
	 encode_cwt_directly(&payload, GRANT_TYPE, dstr); // password
	 encode_cwt_directly(&payload, ACCESS_TOKEN, (uint8_t *) access_token);
	 encode_cwt_directly(&payload, TOKEN_TYPE, (uint8_t *) token_type);
	 encode_cwt_directly(&payload, USERNAME, (uint8_t *) username);
	 encode_cwt_directly(&payload, PASSWORD, (uint8_t *) password);
	 encode_cwt_directly(&payload, PROFILE, (uint8_t *) profile);

 	uint8_t ipsec_bytes[256];
	uint32_t ipsec_len ;
  	const uint8_t encr_key[] =
	  {
	    0x3b, 0xda, 0x5b, 0x6c, 0x05, 0x59, 0x5d, 0xe5,
	    0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xd1, 0xaf, 0xd4,
	    0xd4, 0xa8, 0x07, 0x59, 0x0
	  };
    ipsec_len = generate_ipsec( ipsec_bytes,
	                       "transport",
	                       "ESP",
	                       1235,
	                       "a.b.c.d1",
	                       "a.b.c.d3",
	                       1,
	                       2,
	                       SA_PROTO_ESP,
	                       SA_ENCR_AES_CTR,// SA_PROTO_ESP,
	                       (char *)encr_key
	                       );

	uint8_t cose_key_bytes[256];
	uint16_t cose_key_len;
    cose_key_len = generate_cose_key( cose_key_bytes,
		                      "Symmetric",
		                      "919191",
		                      "aa280649d445");

	uint8_t cnf_bytes[256];
	uint32_t cnf_len;
    cnf_len = generate_cnf(cnf_bytes,
    					cose_key_len,
                        cose_key_bytes,
                        "1327",
                        "1001",
                        ipsec_len,
                        ipsec_bytes,
                      	"ikv2");
	 encode_struct_in_cwt_directly(&payload, CNF, (uint8_t *) cnf_bytes,cnf_len);
	 return payload.ptr;
}

uint16_t generate_dummy_access_token(uint8_t *payload_bytes) {
 	uint32_t time = 3600;
 	uint32_t grant_type = 10;
	return generate_access_token(payload_bytes,
								 "subject",
								 "coap://example.com",
								 time,
								 "a_client_id",
								 "read_scope",
								 grant_type,
								 "an_encoded_access_token",
								 "P-o-P",
								 "client_username",
								 "client_password",
								 "a_cnf",
								 "coap_ipsec");
}



