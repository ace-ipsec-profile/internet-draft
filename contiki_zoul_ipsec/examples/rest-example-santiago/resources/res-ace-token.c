/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Example resource
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdlib.h>
#include <string.h>
#include "er-coap-engine.h"
#include "rest-engine.h"




#include <stdio.h> /* For printf() */
#include "uip-udp-packet.h"
#include "uip.h"
// #include "ipsec.h"
// #include "sad-conf.c"
#include "sad.h"
// #include "ipsec.h"
// #include "ipsec-conf.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip.h"


#include "ace-token.h"

static void res_post_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(res_ace_token,
         "title=\"Access Token Endpoint",
         NULL,
         res_post_handler,
         NULL,
         NULL);


// PROCESS ACCESS TOKEN GOT BY POST REQUEST
static void
res_post_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{


  printf("POST TOKEN RECEIVED\n");
  const uint8_t *payload = NULL;
  coap_get_payload(request, &payload);

   cbor_data cb_data;
   cb_data.buf =  payload;
   cb_data.ptr = 0;

   ipsec_sa new_ipsec_sa;
   decode_cbor_web_token(&cb_data, &new_ipsec_sa);

  static uip_ip6addr_t ip_other;
  static uip_ip6addr_t ip_me;
  const uint8_t encr_key[] =
  // {
  //   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xa1, 0x00, 0x00,0x00, 0x00, 0x07, 0x59, 0x0
  // };
  //
  {
    0x3b, 0xda, 0x5b, 0x6c, 0x05, 0x59, 0x5d, 0xe5, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xd1, 0xaf, 0xd4,0xd4, 0xa8, 0x07, 0x59
  };
  printf("\n\n Key %s\n",(char *)encr_key );
  uip_ip6addr(&ip_me,    0xaaaa, 0, 0, 0x0, 0x212, 0x4b00, 0x9df, 0x4f16);
  uip_ip6addr(&ip_other, 0xaaaa, 0, 0, 0x0, 0x0, 0x0, 0x0, 0x1);
  printf("Initializing SPD...\n");
  printf("Setting SA...\n");
//   // sad_conf();
  // sad_set(
  //       &ip_other,
  //       &ip_me,
  //       &encr_key,
  //       sizeof(encr_key),
  //       1,
  //       2,// 2,
  //       SA_PROTO_ESP,//new_ipsec_sa.prot_type, // SA_PROTO_ESP,
  //       SA_ENCR_AES_CTR);//new_ipsec_sa.encr_alg);// SA_ENCR_AES_CTR);
printf("Setting SA. DONE :D\n");
  // }
  // else{
  //   printf("%d %d",sizeof(received_token),sizeof(valid_token));
  //   printf("No AT processed\n");
  // }

  // printf("REQUEST PAYLOAD: %*s\n",t , (char *)payload);
  // printf("REQUEST PAYLOAD: %*s\n",sizeof(received_token) , (char *)received_token);

  // printf("REQUEST PAYLOAD: %*s\n",sizeof(valid_token) , (char *)valid_token);
  const char *len = NULL;

  char const *const message = "Token received\n";
  int length = strlen(message);
  if(REST.get_query_variable(request, "len", &len)) {
    length = atoi(len);
    if(length < 0) {
      length = 0;
    }
    if(length > REST_MAX_CHUNK_SIZE) {
      length = REST_MAX_CHUNK_SIZE;
    }
    memcpy(buffer, message, length);
  } else {
    memcpy(buffer, message, length);
  } REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_header_etag(response, (uint8_t *)&length, 1);
  REST.set_response_payload(response, buffer, length);
}

